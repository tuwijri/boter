from pyrogram import Client, filters
from pyrogram.errors import UserIsBlocked
from boter.utils.errors import send_error
from boter.config import CMD
from boter.utils.filters import GROUP_ADMINS, FROM_BOT, CHECK_COMMAND
from boter.utils.database import message_from_group_admin, check_message_from_private


@Client.on_message(GROUP_ADMINS & filters.reply & FROM_BOT & ~CHECK_COMMAND)
async def forward_from_admin(client, message):
    try:
        if message.reply_to_message.forward_from:
            message_id = message.id
            chat_id = message.chat.id
            from_user = message.reply_to_message.forward_from.id
            result = await client.copy_message(from_user, chat_id, message_id)
            await message_from_group_admin(client, message, result)
        else:
            message_id = message.reply_to_message.message.id
            from_user = await check_message_from_private(client, message_id)
            from_user = from_user['user_id']
            message_id = message.id
            chat_id = message.chat.id
            result = await client.copy_message(from_user, chat_id, message_id)
            await message_from_group_admin(client, message, result)
    except UserIsBlocked:
        await message.reply("**⚠️ المستخدم قام بحظر البوت**")
    except Exception as error:
        await send_error(client, error, "forward_from_admin sendToChat.py")
