import json
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from pyrogram import filters
from boter.boter import Boter
from boter.config import CMD, BOT_USERNAME
from boter.utils.errors import send_error
from boter.utils.database import activate_group, set_permission_group, check_group_admins
from boter.utils.filters import IS_ADMIN



# @Boter.on_callback_query(filters.regex("activate_groups_lists"))
# async def activate_groups_lists(client, callback_query):
#     """
#         اعدادات
#         BOT_USERNAME
#     """
#     try:
#         button_data = callback_query.data
#         # print(callback_query)
#         # data = button_data.split("groups_settings#")[1]
#         message_id = callback_query.message.id
#         chat_id = callback_query.message.chat.id
#         group_message = f"⚙️-اضافة البوت لمجموعة جديدة تريد إدارتها\n-"
#         # data = ["addGroup",message_id,chat_id]
#         data = f"addGroup@{message_id}@{chat_id}"
#         keyboard = InlineKeyboardMarkup(
#             [
#                 [
#                     InlineKeyboardButton(
#                         text="أختر المجموعة المراد إدارتها",
#                         url=f"http://t.me/{BOT_USERNAME}?startgroup=addGroup_{message_id}_{chat_id}"
#                     )
#                 ],
                 
                
#             ]
#         )
#         await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
#     except Exception as error:
#         await send_error(client, error, "activate_groups_lists active.py")


@Boter.on_message(filters.command("start") & filters.group)
async def activate_groups(client, message):
    
    split_message = str(message.command[1]).split("_")
    message_id = split_message[1]
    chat_id = split_message[2]
    print(split_message)
    if message.chat.type != "private" and split_message[0] == "addGroup":
        try:
            # permissions_group = await client.get_chat(message.chat.id)
            # permissions = json.loads(f"{permissions_group.permissions}")
            # del permissions["_"]
            active = await activate_group(client, message)
            group_admins = check_group_admins(client)
            if group_admins != "NO_GROUP":
                try:
                    await client.send_message(group_admins,active)
                    await client.delete_messages(int(chat_id), int(message_id))
                finally:
                    pass
        except Exception as error:
            await send_error(client, error, "activate_groups active.py")




# @Boter.on_message(filters.command(['addGroup']))
# async def activate_groups(client, message):
#     try:
#         print(message)
#         permissions_group = await client.get_chat(message.chat.id)
#         permissions = json.loads(f"{permissions_group.permissions}")
#         del permissions["_"]
#         active = await activate_group(client, message, permissions)
#         await message.reply(active)
#     except Exception as error:
#         await send_error(client, error, "activate_groups active.py")
