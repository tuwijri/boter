from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, remove_all_block_user
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("removeAllBlock"))
async def remove_all_allowed_settings(client, callback_query):
    try:
        button_data = callback_query.data
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_message = "**⚠️ حذف جميع المحظورين :**"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "❌ الغاء",
                            callback_data=f"blocked_settings"
                        ),
                        InlineKeyboardButton(
                            "✅ تأكيد",
                            callback_data=f"deleteAllBlocked"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "remove_all_allowed_settings removeAllAllowed.py")


@Boter.on_callback_query(filters.regex("deleteAllBlocked"))
async def delete_all_allowed(client, callback_query):
    try:
        button_data = callback_query.data
        is_removed = await remove_all_block_user(client)
        if is_removed == "DONE":
            group_message = "**☑️ تم حذف جميع المحظورين**"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"blocked_settings"
                        ),
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
        else:
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"blocked_settings"
                        ),
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{is_removed}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "delete_all_allowed removeAllAllowed.py")