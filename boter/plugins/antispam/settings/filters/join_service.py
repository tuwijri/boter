from boter.boter import Boter
from pyrogram import filters
from boter.utils.database import check_group, get_group_settings, get_media_state
from boter.utils.errors import send_error
from boter.utils.filters import GROUP_ADMINS, IS_ADMIN


@Boter.on_message(filters.new_chat_members & ~GROUP_ADMINS & ~IS_ADMIN & filters.group)
async def filter_join_service(client, message):
    try:
        chat_id = message.chat.id
        is_group = await check_group(client, chat_id)

        if is_group != "NO_GROUP":
            group_data = await get_group_settings(client, chat_id)
            welcome_message = group_data['welcome']['message']
            welcome_status = group_data['welcome']['active']
            join_service = get_media_state(client, chat_id, "join_service")
            if welcome_message != "NO_MESSAGE" and welcome_status == "YES":
                await client.send_message(chat_id, f"{welcome_message}")
                if join_service == "❌":
                    try:
                        await message.delete()
                    finally:
                        pass
            else:
                if join_service == "❌":
                    try:
                        await message.delete()
                    finally:
                        pass
    except Exception as error:
        await send_error(client, error, "filter_join_service join_service.py")
