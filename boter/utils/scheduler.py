import datetime
from boter.config import CHANNEL_LOG
from .database import set_timer_lock, set_to_time_unlock, set_from_time_unlock, get_message_lock
from .errors import send_error
from boter.utils.check_timers import scheduler
from boter.plugins.antispam.settings.permissions.PermissionsGroup import close_group, open_group


async def set_none_total_time(client, group_id):
    """
    - cancel_permission : بعد فتح القروب يتم ارجاع قيمة المؤقت الى none
    """
    try:
        await set_timer_lock(client, group_id, "None")
    except Exception as error:
        await send_error(client, error, "set_none_total_time scheduler.py")


async def Permission(status, hour, minute, group_id, client):
    """
    - Permission : جدولة قفل او فتح القروب
        > hour : وقت المهمة في اليوم بالساعات
        > minute : وقت المهمة في اليوم بالدقائق
        > group_id : ايدي القناة التي ستتم فيها المهمة
        > client : لارسال الرسائل بإستخدام دالة send_error
    """
    try:
        if status == "timeClose":
            message_type = "lock_message"
            chek_id_job = scheduler.get_job(f'{status}Group#{group_id}')
            if chek_id_job is not None:
                scheduler.remove_job(f'{status}Group#{group_id}')
            scheduler.add_job(close_group, 'cron', hour=hour, minute=minute, args=[client, group_id, message_type],
                              timezone="Asia/Riyadh",
                              id=f'{status}Group#{group_id}')

        elif status == "timeOpen":
            message_type = "unlock_message"
            chek_id_job = scheduler.get_job(f'{status}Group#{group_id}')
            if chek_id_job is not None:
                scheduler.remove_job(f'{status}Group#{group_id}')
            scheduler.add_job(open_group, 'cron', hour=hour, minute=minute, args=[client, group_id, message_type],
                              timezone="Asia/Riyadh",
                              id=f'{status}Group#{group_id}')

    except Exception as error:
        await send_error(client, error, "Permission scheduler.py")


async def timer(minutes, group_id, client):
    """
    - timer : مؤقت لقفل القروب بالدقائق
        > minutes : مدة القفل
        > group_id : ايدي القناة التي ستتم فيها المهمة
        > client : لارسال الرسائل بإستخدام دالة send_error
    """
    try:
        permission_group = await client.get_chat(group_id)
        if permission_group.permissions.can_send_messages == True:
            updated_time = datetime.datetime.now() + datetime.timedelta(minutes=minutes)
            await close_group(client, group_id)
            chek_id_job = scheduler.get_job(f'timerGroup#{group_id}')
            if chek_id_job is not None:
                scheduler.remove_job(f'timerGroup#{group_id}')
            scheduler.add_job(open_group, "date", run_date=str(updated_time), args=[client, group_id],
                            id=f'timerGroup#{group_id}')

            chek_id_job = scheduler.get_job(f'setNoneTimerGroup#{group_id}')
            if chek_id_job is not None:
                scheduler.remove_job(f'setNoneTimerGroup#{group_id}')
            scheduler.add_job(set_none_total_time, "date", run_date=str(updated_time), args=[client, group_id],
                            id=f'setNoneTimerGroup#{group_id}')
            await set_timer_lock(client, group_id, minutes)
            return f"تم قفل القروب لمدة : {minutes}"
        elif permission_group.permissions.can_send_messages == False:
            return "المجموعة مغلقة من قبل"
    except Exception as error:
        await send_error(client, error, "timer scheduler.py")


async def cancel_permission(group_id, client):
    """
    - cancel_permission : الغاء الكرون جوب
        > group_id : ايدي القناة التي ستتم فيها المهمة
        > client : لارسال الرسائل بإستخدام دالة send_error
    """
    try:
        await set_from_time_unlock(client, group_id)
        await set_to_time_unlock(client, group_id)
        Open_permission = scheduler.get_job(f'timeOpenGroup#{group_id}')
        Close_permission = scheduler.get_job(f'timeCloseGroup#{group_id}')
        if Open_permission is not None:
            scheduler.remove_job(f'timeOpenGroup#{group_id}')
        if Close_permission is not None:
            scheduler.remove_job(f'timeCloseGroup#{group_id}')

    except Exception as error:
        await send_error(client, error, "cancel_permission scheduler.py")
