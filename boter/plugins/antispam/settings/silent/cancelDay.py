from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error
from boter.utils.scheduler import timer, cancel_permission


@Boter.on_callback_query(filters.regex("cancelDay"))
async def accept_cancel_day(client, callback_query):
    try:
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("cancelDay#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"👥--هل انت متأكد من الغاء توقيت فتح وقف المجموعة :-- **{group_name}**\n-"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "نعم",
                            callback_data=f"yesCancelDay#{data}"
                        ),
                        InlineKeyboardButton(
                            "لا",
                            callback_data=f"silent_settings#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"silent_settings#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "accept_cancel_day cancelDay.py")


@Boter.on_callback_query(filters.regex("yesCancelDay"))
async def cancel_day(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("yesCancelDay#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"تم الغاء جميع التواقيت للمجموعة **{group_name}**\n-"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"silent_settings#{data}"
                        )
                    ],
                ]
            )
            await cancel_permission(data, client)
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "cancel_day cancelDay.py")
