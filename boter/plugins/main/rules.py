from pyrogram import filters
from boter.boter import Boter
from boter.config import CMD
from boter.utils.errors import send_error
from boter.utils.database import check_group, get_group_settings
from boter.utils.filters import IS_ADMIN


@Boter.on_message(filters.command(['القوانين'], prefixes=CMD) & IS_ADMIN)
async def send_rules(client, message):
    try:
        chat_id = message.chat.id
        is_group = await check_group(client, chat_id)
        group_data = await get_group_settings(client, chat_id)
        rules_status = group_data['rules']['active']
        rules_message = group_data['rules']['message']
        if is_group != "NO_GROUP" and rules_status == "YES" and rules_message != "NO_MESSAGE":
            await message.reply(rules_message)

    except Exception as error:
        await send_error(client, error, "send_rules rules.py")
