from boter.plugins.antispam.settings.permissions.PermissionsGroup import close_group, open_group
from boter.utils.database import get_groups, set_timer_lock
from apscheduler.schedulers.asyncio import AsyncIOScheduler
import datetime
from boter.config import CHANNEL_LOG
from boter.utils.errors import send_error

scheduler = AsyncIOScheduler()

try:
    scheduler.start()
except (KeyboardInterrupt, SystemExit):
    pass


async def check_timer(client):
    try:
        list_groups = get_groups()
        if list_groups != "NO_GROUPS":
            for group in list_groups:
                group_id = group['group_id']
                from_time = group['silent']['from_time']
                Open_permission = scheduler.get_job(f'timeOpenGroup#{group_id}')
                Close_permission = scheduler.get_job(f'timeCloseGroup#{group_id}')
                if from_time != "None":
                    if Open_permission is not None:
                        scheduler.remove_job(f'timeOpenGroup#{group_id}')
                    hour = from_time.split(":")[0]
                    minute = from_time.split(":")[1]
                    message_type = "lock_message"
                    scheduler.add_job(close_group, 'cron', hour=hour, minute=minute, args=[client, group_id, message_type], timezone="Asia/Riyadh", id=f'timeOpenGroup#{group_id}')
                to_time = group['silent']['to_time']
                if to_time != "None":
                    if Close_permission is not None:
                        scheduler.remove_job(f'timeCloseGroup#{group_id}')
                    hour = to_time.split(":")[0]
                    minute = to_time.split(":")[1]
                    message_type = "unlock_message"
                    scheduler.add_job(open_group, 'cron', hour=hour, minute=minute, args=[client, group_id, message_type], timezone="Asia/Riyadh", id=f'timeCloseGroup#{group_id}')
                total_time = group['silent']['total_time']
                if total_time != "None":
                    await client.send_message(CHANNEL_LOG, "تعطل القفل المؤقت أرجو الرجوع وضبطه مرة اخرى", parse_mode="Markdown")
                    await set_timer_lock(client, group_id, "None")
    except Exception as error:
        await send_error(client, error, "check_timer check_timers.py")
