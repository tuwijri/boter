from pyrogram import filters
from boter.boter import Boter
from boter.utils.errors import send_error
from boter.utils.database import delete_admin_message


@Boter.on_callback_query(filters.regex('to_delete_'))
async def deleted_message(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("to_delete_")[1]
        delete_message = await delete_admin_message(client, data)
        if delete_message != "NOT_DONE":
            await callback_query.edit_message_text(text=f"🗑--تم حذف الرسالة:--\n {delete_message}")
        else:
            await callback_query.edit_message_text(text="**⚠️ هناك خطأ لم يتم حذف الرسالة.**")
    except Exception as error:
        await send_error(client, error, "deleted_message delete_buttons.py")
