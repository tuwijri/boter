from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, get_permission_group, change_permission_setting
from boter.plugins.antispam.settings.permissions.PermissionsGroup import close_group, open_group
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("edit_silent_settings"))
async def edit_silent_settings_(client, callback_query):
    try:
        button_data = callback_query.data
        group_id = button_data.split("#")[1]
        active = button_data.split("#")[2]
        lock_status = await client.get_chat(group_id)
        if active == "active":
            if lock_status.permissions.can_send_messages:
                await close_group(client, int(group_id))
            elif not lock_status.permissions.can_send_messages:
                await open_group(client, int(group_id))
    except:
        pass
    try:
        button_data = callback_query.data
        group_id = button_data.split("#")[1]
        group_data = await get_group_settings(client, int(group_id))
        is_group = await check_group(client, int(group_id))
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"👥--اعدادات قفل المجموعة :-- **{group_name}**\n-"
            lock_status = await client.get_chat(int(group_id))
            silent_status = "🔔" if lock_status.permissions.can_send_messages == True else "🔕"
            keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                f"🔘 حالة القفل : {silent_status}",
                                callback_data=f"edit_silent_settings#{group_id}#active"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "📆 قفل يومي",
                                callback_data=f"EveryDay#{group_id}"
                            ),
                            InlineKeyboardButton(
                                "⏰ قفل مؤقت",
                                callback_data=f"TimerLock#{group_id}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🏷 ضبط الصلاحيات",
                                callback_data=f"LockGroupList#{group_id}#New"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "📨 ضبط رسائل القفل",
                                callback_data=f"messageLock#{group_id}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"groups_settings#{group_id}"
                            )
                        ],
                    ]
                )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client,error, "silent_settings silentSettings.py")
