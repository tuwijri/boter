from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, remove_allowed_words
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("removeAllowedWord"))
async def remove_allowed_settings(client, callback_query):
    try:
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("removeAllowedWord#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_message = "⏳ أرسل الكلمة التي تريد السماح بها .."
            bot_reply = await callback_query.edit_message_text(f"{group_message}\n- أرسل الامر : /cancel للإلغاء\n-")
            reply = await message.chat.listen(filters.text & filters.user(user_id))
            text = reply.text
            bot_username = await client.get_me()
            cancel_word = [
                    "/cancel", f"/cancel@{bot_username.username}",
                    "#cancel", f"#cancel@{bot_username.username}",
                    "#الغاء", f"#الغاء@{bot_username.username}",
                    "/الغاء", f"/الغاء@{bot_username.username}",
                    ""
                           ]
            if text in cancel_word:
                message.chat.cancel_listener()
                await bot_reply.delete()
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"allowed_words_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply('**✅ تم الإلغاء**', reply_markup=keyboard)
            else:
                message_text = await remove_allowed_words(client, data, text)
                await bot_reply.delete()
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"allowed_words_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply(message_text, reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "remove_allowed_settings removeAllowedWord.py")
