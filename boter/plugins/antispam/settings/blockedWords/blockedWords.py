from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("blocked_words_"))
async def blocked_words_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("blocked_words_")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_data = await get_group_settings(client, data)
            group_name = group_data['group_name']
            blocked_status = group_data['blocked_words']['active']
            group_message = f"🚫--اعدادات الكلمات المحظورة :-- **{group_name}**\n-"
            banded_active = "❌" if blocked_status == "NO" else "✅"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            f"♻️ حالة الكلمات :  {banded_active}",
                            callback_data=f"blockedActive#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "📃 الكلمات المحظورة",
                            callback_data=f"listBlockedWords#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "➕",
                            callback_data=f"addBlockedWord#{data}"
                        ),
                        InlineKeyboardButton(
                            "➖",
                            callback_data=f"removeBlockedWord#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "⚠️ ازالة جميع الكلمات",
                            callback_data=f"removeAllBlocked#{data}"
                        )
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"groups_settings#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "blocked_words_settings blockedWords.py")
