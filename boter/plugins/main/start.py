import json
from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.filters import GROUP_ADMINS, IS_ADMIN
from boter.config import ADMINS, CMD, BOT_USERNAME
from boter.utils.errors import send_error
from boter.utils.database import new_user, check_admin, check_rules, check_group_admins, activate_group, set_permission_group

WELCOME_USER = "اهلا بك في البوت 😄"  # رسالة الترحيب للاعضاء
WELCOME_ADMIN = "اهلا بك في لوحة التحكم 😄"  # رسالة الترحيب للمشرفين





def extract_unique_code(text):
    # Extracts the unique_code from the sent /start command.
    return text.split()[1] if len(text.split()) > 1 else None



# امر /start في الخاص
@Boter.on_message(filters.command(['start']) & filters.private)
async def start_command(client, message):
    try:
        split_message = message.text.split()
        if len(split_message) > 1:
            rules = split_message[1]
            if rules.startswith("rules_"):
                rules_message = rules.split("rules_")
                if rules_message[1]:
                    is_rules = await check_rules(client, rules_message[1])
                    await message.reply(f"{is_rules['message']}")
        else:
            await new_user(client, message)  # حفظ بيانات العضو
            user_id = message.from_user.id
            admin = check_admin(client, user_id)
            # اذا كانت الرسالة من مشرف
            if admin == "ADMIN" or user_id in ADMINS:
                await message.reply(WELCOME_ADMIN)
            else:
                await message.reply(WELCOME_USER)  # ارسال رسالة الترحيب
    except Exception as error:
        await send_error(client, error, "start_command start.py")








# @Boter.on_message(filters.command(['start']) & filters.group & IS_ADMIN)
# async def start_command(client, message):
#     try:

#     except Exception as error:
#         await send_error(client, error, "start_command start.py")


# @Boter.on_message(filters.command(['add']))
# async def start_message(client, message):
#     keyboard = InlineKeyboardMarkup()  # Keyboard
#     key_add_to_group = InlineKeyboardButton(text='Add to group', url='http://t.me/evil_cards_bot?startgroup=test')
#     keyboard.add(key_add_to_group)  # Add button to keyboard
#     response_text = 'Add bot to group.'
#     # bot.send_message(message.chat.id, text=response_text, reply_markup=keyboard)
#     await message.reply(reply_markup=keyboard)



