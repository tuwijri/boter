import re
from boter.boter import Boter
from pyrogram import filters
from boter.utils.database import (check_blocked_status, check_group_admins,
                                  check_group, get_media_state, check_lang_status, check_blocked_word,
                                  check_allowed_status)
from boter.utils.errors import send_error
from boter.utils.filters import GROUP_ADMINS, IS_ADMIN
from pyrogram.errors import UserNotParticipant


########################################
## حاليا لا يمنع رقم الجوال ولا الروابط ##
########################################
@Boter.on_message(filters.caption & ~GROUP_ADMINS & ~IS_ADMIN & filters.group)
async def filter_caption(client, message):

    try:
        user_id = message.from_user.id
        chat_id = message.chat.id
        is_group = await check_group(client, chat_id)
        if is_group != "NO_GROUP":
            group_admins = check_group_admins(client)
            text = get_media_state(client, chat_id, "text")
            if group_admins != "NO_GROUP":
                try:
                    group_admins_users = await client.get_chat_member(group_admins, user_id)
                    user_group = group_admins_users.user.id
                    admins = await client.get_chat_member(chat_id, user_id)
                    blocked = await check_blocked_status(client, chat_id)
                    if blocked == "ACTIVE":
                        check_word = await check_blocked_word(client, chat_id, message.caption)
                        if check_word == "BLOCKED":
                            try:
                                await message.delete()
                            finally:
                                pass                 
                except UserNotParticipant:
                    admins = await client.get_chat_member(chat_id, user_id)
                    blocked = await check_blocked_status(client, chat_id)
                    if blocked == "ACTIVE":
                        check_word = await check_blocked_word(client, chat_id, message.caption)
                        if check_word == "BLOCKED":
                            try:
                                await message.delete()
                            finally:
                                pass         
            else:
                admins = await client.get_chat_member(chat_id, user_id)
                blocked = await check_blocked_status(client, chat_id)
                if blocked == "ACTIVE":
                    check_word = await check_blocked_word(client, chat_id, message.caption)
                    if check_word == "BLOCKED":
                        try:
                            await message.delete()
                        finally:
                            pass

    except Exception as error:
        await send_error(client, error, "filter_caption caption.py")
