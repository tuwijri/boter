from .errors import send_error
from datetime import datetime
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from pymongo import MongoClient
from ..config import DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_HOST, BOT_USERNAME, DATABASE_PORT


DATABASE_PORT = int(DATABASE_PORT)

if DATABASE_USERNAME is not None:

    client = MongoClient(host=DATABASE_HOST,
                        port=DATABASE_PORT,
                        username=DATABASE_USERNAME, 
                        password=DATABASE_PASSWORD,
                    authSource="admin")      
else:
    client = MongoClient(f'mongodb://{DATABASE_HOST}:{DATABASE_PORT}')

database = client[DATABASE_NAME]
users = database["Users"]  # الاعضاء
groups = database["Groups"]  # المجموعات
admins = database["Admins"]  # المشرفين
group_admins = database["GroupAdmins"]  # مجموعة المشرفين
messageFromAdmin = database["messageFromAdmin"]  # الرسائل المرسلة من قروب المشرفين support
block_user = database["blockUser"] # المستخدمين المحظورين
messageFromPrivate = database["messageFromPrivate"] # الرسائل المرسلة من المستخدمين

async def new_user(client, message):
    """
    - new_user : حفظ معلومات العضو عند ارسال الامر /start في الخاص
        > client : لارسال الرسالة, بإستخدام الدالة send_message
        > message : لاستخراج معلومات العضو
    """
    try:
        first_name = message.from_user.first_name  # الاسم الاول
        last_name = message.from_user.last_name if message.from_user.last_name else "❌"  # الاسم الثاني
        user_name = message.from_user.username if message.from_user.username else "❌"  # المعرف
        user_id = message.from_user.id  # الايدي
        now = datetime.now()  # الوقت / التاريخ
        join_date = now.strftime("%Y-%m-%d %H:%M:%S")  # تاريخ الانضمام

        user_info = {
            "first_name": first_name,
            "last_name": last_name,
            "user_name": user_name,
            "user_id": user_id,
            "join_date": join_date
        }
        is_user = users.find_one({"user_id": int(user_id)})
        if not is_user:
            users.insert_one(user_info)
    except Exception as error:
        await send_error(client, error, "new_user database.py")


async def activate_group(client, message):
    """
        - active_group : تفعيل البوت في المجموعة
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > message : لاستخراج معلومات العضو
    """
    try:
        group_name = message.chat.title  # اسم المجموعة
        group_id = message.chat.id  # ايدي المجموعة
        group_type = str(message.chat.type)  # نوع المجموعة group - supergroup
        group_type = group_type.split(".")[1]  # نوع المجموعة group - supergroup
        user_id = message.from_user.id  # ايدي المشرف الذي قام بتفعيل المجموعة
        user_name = message.from_user.first_name  # اسم المشرف الذي قام بتفعيل المجموعة
        
        media_settings = {
            "document": "✅",  # الملفات
            "photo": "✅",  # الصور
            "video": "✅",  # الفيديو
            "voice": "✅",  # تسجيلات الصوت
            "audio": "✅",  # ملفات صوتية
            "sticker": "✅",  # ملصقات
            "video_note": "✅",  # ملاحظات فيديو
            "gif": "✅",  # صور متحركة
            "forward": "✅",  # اعادة التوجيه
            "telegram_link": "✅",  # روابط التليجرام
            "link": "✅",  # الروابط العامة
            "mobile": "✅",  # رقم الجوال بالرسالة
            "tag": "✅",  # التاق
            "hashtag": "✅",  # الهاشتاق
            "bots": "✅",  # البوتات
            "join_service": "✅",  # اشعارات الدخول
            "left_service": "✅",  # اشعارات الخروج
            "location": "✅",  # المواقع
            "games": "✅",  # الالعاب
        }

        # اعدادات المجموعة
        group_info = {
            'group_name': group_name,  # اسم المجموعة
            'group_id': group_id,  # ايدي المجموعة
            'group_type': group_type,  # نوع المجموعة
            'user_name': user_name,  # اسم المشرف الذي قام بتفعيل المجموعة
            'user_id': user_id,  # ايدي المشرف الذي قام بتفعيل المجموعة
            'control_panel_place': "GROUP",  # مكان ارسال لوحة التحكم
            'captcha': "NO",  # التحقق من العضو
            'media_settings': media_settings,  # اعدادات الوسائط
            'blocked_words': {
                'active': "NO",  # الكلمات المحظورة
                'blocked_list': []
            },
            'silent': {
                'active': "NO",  # حالة القفل
                'lock_message': "🔕 تم قفل المجموعة",  # رسالة القفل
                'unlock_message': "🔔 تم الغاء قفل المجموعة",  # رسالة الغاء القفل
                'timer_lock_message': "🔕 تم قفل المجموعة",  # رسالة القفل المؤقت
                'ad_lock_message': "حياكم في قناتنا",  # رسالة القفل المؤقت
                'from_time': "None",  # من الساعة x للقفل المؤقت
                'to_time': "None",  # الى الساعة x للقفل المؤقت
                'total_time': "None",  # عدد ساعات القفل
            },
            'warn': {
                'active': "NO",  # حالة التحذيرات
                'warns': 3,  # الحد الاقصى للتحذيرات
                'action': "KICK",  # اجراء التحذيرات
                'users': []
            },
            'flood': {
                'active': "NO",  # حالة التكرار
                'max': 4,  # الحد الاقصى للتكرار
                'time': 3,  # وقت التكرار
                'action': "KICK",  # اجراء التكرار
                'delete_messages': "YES",  # حذف الرسائل
                'users': []
            },
            'welcome': {
                'active': "NO",  # حالة الرسالة
                'message': "NO_MESSAGE",  # الرسالة الافتراضية
                'delete_last_message': "NO",  # حذف اخر رسالة ترحيب
                'last_message_id': "None",  # ايدي اخر رسالة ترحيب
                'with_rules': "NO",  # رسالة ترحيب مع قوانين المجموعة
                'buttons': []
            },
            'rules': {
                'active': "NO",  # حالة القوانين
                'message': "NO_MESSAGE",  # رسالة القوانين
                'permission': "ADMINS",  # الصلاحيات - للمشرفين او للجميع "اعضاء + مشرفين"
                'place': "GROUP",  # مكان ارسال القوانين "المجموعة - الخاص"
                'buttons': []
            },
            'permission': {
                "can_send_messages": "✅",  # إرسال الرسائل
                "can_send_media_messages": "✅",  # إرسال الوسائط
                "can_send_other_messages": "✅",  # إرسال الملصقات والصور المتحركة
                "can_send_polls": "✅",  # إرسال الاستفتاءات
                "can_add_web_page_previews": "✅",  # معاينة الروابط
                "can_change_info": "✅",  # تغيير معلومات المجموعة
                "can_invite_users": "✅",  # إضافة الأعضاء
                "can_pin_messages": "✅",  # تثبيت الرسائل
            },
        }
        # البحث عن ايدي المجموعة
        info = groups.find_one({'group_id': group_id})
        if not info:
            # اضافة المجموعة ان لم تكن موجودة
            groups.insert_one(group_info)
            return "**✅ تم تفعيل المجموعة**"
        else:
            # اذا كانت المجموعة مفعلة مسبقاً
            return "**❎ المجموعة مفعلة مسبقاً**"
    except Exception as error:
        await send_error(client, error, "activate_group database.py")


async def deactivate_group(client, group_id):
    """
        - deactivate_group: تعطيل البوت في المجموعة
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > message : لاستخراج معلومات العضو
    """
    try:
        group_id = int(group_id)
        info = groups.find_one({'group_id': group_id})
        if info:
            groups.delete_one({'group_id': group_id})
            return "**☑️ تم الغاء تفعيل المجموعة**"
        else:
            return "**⚠️ المجموعة ليست مفعلة**"
    except Exception as error:
        await send_error(client, error, "deactivate_group database.py")


def check_admin(client, user_id):
    """
        - check_admin: التأكد من ان عضو مشرف في البوت
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > user_id : ايدي العضو
    """
    try:
        info = admins.find_one({"user_id": user_id})
        if info:
            return "ADMIN"
        else:
            return "NOT_ADMIN"
    except Exception as error:
        print(f"> Error in check_admin functions in database\n{error}")
        # we can not use it now because we use it in filters so we use print :(
        # await send_error(client, error, "check_admin database.py")


async def new_admin(client, user_id, user_name, first_name, last_name):
    """
        - new_admin: اضافة مشرف جديد للبوت
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > user_id : ايدي العضو
            > user_name : يوزر العضو
            > first_name : اسم العضو
            > last_name : الاسم الاخير للعضو
    """
    try:
        # التأكد اذا كان العضو مشرف او لا
        info = admins.find_one({"user_id": user_id})
        if info:
            return "**⚠️ الضعو تم رفعه مشرف مسبقاً**"
        else:
            # اضافة مشرف جديد
            admin_data = {
                "user_id": user_id,
                "user_name": user_name,
                "first_name": first_name,
                "last_name": last_name,
            }
            admins.insert_one(admin_data)
            return f"**✅ تم اضافة مشرف جديد :** [{first_name}](tg://user?id={user_id})"
    except Exception as error:
        await send_error(client, error, "check_admin database.py")


async def remove_admin(client, user_id):
    """
        - remove_admin: ازالة مشرف من البوت
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > user_id : ايدي المشرف
    """
    try:
        info = admins.find_one({"user_id": user_id})
        if info:
            first_name = info["first_name"]
            admins.delete_one({"user_id": user_id})
            return f"**☑️ تم ازالة المشرف :** [{first_name}](tg://user?id={user_id})"
        else:
            return "**⚠️ العضو ليس مشرف في البوت**"
    except Exception as error:
        await send_error(client, error, "remove_admin database.py")


async def bot_admins(client):
    """
        - bot_admins: قائمة المشرفين في البوت
            > client : لارسال الرسالة, بإستخدام الدالة send_message
    """
    try:
        count_admins = admins.count_documents({})
        if count_admins > 0:
            list_admins = admins.find()
            lists = [f"▫️ [{i['first_name']}](tg://user?id={i['user_id']})" for i in list_admins]
            all_admins = "\n".join(lists)
            admins_message = f"📜 --مشرفين البوت :--\n\n{all_admins}"
            return admins_message
        else:
            return "**❌ لا يوجد مشرفين في البوت**"
    except Exception as error:
        await send_error(client, error, "bot_admins database.py")


def check_group_admins(client):
    """
        check_group_admins: التأكد اذا كانت مجموعة المشرفين موجودة او لا
            > client : لارسال الرسالة, بإستخدام الدالة send_message
    """
    try:
        admins_group = group_admins.find_one()
        if admins_group:
            admins_group = group_admins.find_one()
            return admins_group['group_id']
        else:
            return "NO_GROUP"
    except Exception as error:
        print(f"> Error in check_group_admins functions in database\n{error}")
        # we can not use it now because we use it in filters so we use print :(
        # await send_error(client, error, "check_group_admins database.py")


async def update_group_admins(client, message, chat_id):
    """
        update_group_admins: تعيين مجموعة المشرفين - تحديثها اذا كانت موجودة
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > message : معلومات المجموعة - اسم + ايدي
            > group_id : ايدي المجموعة القديمة
    """
    try:
        new_chat_id = message.chat.id
        chat_name = message.chat.title
        if chat_id != new_chat_id:
            group_admins.drop()
            group_info = {
                "group_name": chat_name,
                "group_id": new_chat_id,
            }
            group_admins.insert_one(group_info)
            return "**✅ تم تحديث مجموعة المشرفين**"
        else:
            return "**⚠️ هذه المجموعة للمشرفين**"
    except Exception as error:
        await send_error(client, error, "update_group_admins database.py")


async def set_group_admins(client, message):
    """
        set_group_admins: تعيين مجموعة المشرفين - تحديثها اذا كانت موجودة
            > client : لارسال الرسالة, بإستخدام الدالة send_message
            > message : معلومات المجموعة - اسم + ايدي
            > group_id : ايدي المجموعة القديمة
    """
    try:
        chat_id = message.chat.id
        chat_name = message.chat.title
        group_info = {
            "group_name": chat_name,
            "group_id": chat_id,
        }
        group_admins.insert_one(group_info)
        return "**✅ تم تعيين مجموعة المشرفين**"
    except Exception as error:
        await send_error(client, error, "set_group_admins database.py")


async def message_from_group_admin(client, message, result):
    """
        تسجيل الرسائل المرسلة من قروب المشرفين للرد على استفسارات الأعضاء للتمكن من حذفها عند الحاجة
    """
    try:
        chat_id = result.chat.id
        message_id = result.id
        message = result.text
        if message is None and result.media is True:
            message = 'الرسالة عبارة عن ملف ميديا'
        text_message = {
            "chat_id": chat_id,
            "message_id": int(message_id),
            "message": message
        }
        messageFromAdmin.insert_one(text_message)
    except Exception as error:
        await send_error(client, error, "message_from_group_admin database.py")


async def get_messages_database(client):
    """
        استخراج اخر 5 رسائل من قواعد البيانات
    """
    try:
        messages = messageFromAdmin.find().limit(10).sort("message_id", -1)
        if messages:
            return messages
        else:
            return "NO_MESSAGES"
    except Exception as error:
        await send_error(client, error, "get_message_database database.py")


async def group_lists(client, callback_query):
    """
        قائمة المجموعات المفعلة
    """
    try:
        message_id = callback_query.message.id
        chat_id = callback_query.message.chat.id
        count_groups = groups.count_documents({})
        if count_groups > 0:
            list_groups = groups.find()
            keyboard_list = []
            
            keyboard = InlineKeyboardMarkup(
                keyboard_list,
            )
            for i in list_groups:
                keyboard_list.append(
                    [
                        InlineKeyboardButton(
                            text=f"{i['group_name']}",
                            callback_data=f"groups_settings#{i['group_id']}"
                        )
                    ]
                )
            keyboard_list.append(
                [
                    InlineKeyboardButton(
                        text=f"➕ اضافة مجموعة جديدة",
                        url=f"http://t.me/{BOT_USERNAME}?startgroup=addGroup_{message_id}_{chat_id}"
                    )
                ]
            )
            keyboard_list.append(
                
                [
                    InlineKeyboardButton(
                        text="🔙 الرجوع الى الاعدادات الرئيسة",
                        callback_data=f"Back_settings_bot"
                    )
                ]
            )
            keyboard_list.append(

                [
                    InlineKeyboardButton(
                        text=" ❌ الخروج من الإعدادات",
                        callback_data="exit_settings"
                    )
                ]
            )
            return keyboard
        else:
            return "NO_GROUPS"
    except Exception as error:
        await send_error(client, error, "group_lists database.py")


async def check_group(client, group_id):
    """
        التأكد اذا المجموعة مفعلة او لا
    """
    try:
        is_group = groups.find_one({"group_id": int(group_id)})
        if is_group:
            return "IS_GROUP"
        else:
            return "NO_GROUP"
    except Exception as error:
        await send_error(client, error, "check_group database.py")


async def get_group_settings(client, group_id):
    """
        الحصول على معلومات المجموعة الرئيسية
    """
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            return group_info
        else:
            return "NO_GROUP"
    except Exception as error:
        await send_error(client, error, "get_group_settings database.py")


async def get_media_settings(client, group_id, media_type):
    """
       الحصول على معلومات اعدادات الوسائط
    """
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        return group_info['media_settings'][media_type]
    except Exception as error:
        await send_error(client, error, "get_media_settings database.py")


async def change_media_setting(client, group_id, media_type):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info['media_settings'][media_type]
        if type_state == "✅":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"media_settings.{media_type}": "❌"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"media_settings.{media_type}": "✅"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_media_setting database.py")


async def get_language_settings(client, group_id, language_type):
    """
       الحصول على معلومات اعدادات اللغة
    """
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        return group_info["language_settings"][language_type]
    except Exception as error:
        await send_error(client, error, "get_language_settings database.py")


async def change_language_setting(client, group_id, language_type):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info["language_settings"][language_type]
        if type_state == "✅":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"language_settings.{language_type}": "❌"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"language_settings.{language_type}": "✅"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_language_setting database.py")


def get_media_state(client, group_id, media_type):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info['media_settings'][media_type]
        return type_state
    except Exception as error:
        print(error)
        # we can not use it now because we use it in filters so we use print :(
        # await send_error(client, error, "get_media_state database.py")


# async def get_permission_group(group_id, client):
#     """
#         جلب اعدادات الامان والقفل القروب من قواعد البيانات
#     """
#     try:
#         find_group = groups.find_one({"group_id": int(group_id)})
#         if find_group:
#             return find_group["permission"]
#         else:
#             return "NO_GROUP"
#     except Exception as error:
#         await send_error(client, error, "get_permission_group database.py")


async def set_permission_group(group_id, per_group, client):
    """
        تسجيل صلاحيات القروب في قواعد البيانات
    """
    try:
        find_group = groups.find_one({"group_id": int(group_id)})
        if find_group:
            status1 = {"group_id": group_id}
            status2 = {"$set": {"permission": per_group}}
            groups.update_one(status1, status2)
    except Exception as error:
        await send_error(client, error, "set_permission_group database.py")


async def change_block_status(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info["blocked_words"]["active"]
        if type_state == "YES":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"blocked_words.active": "NO"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"blocked_words.active": "YES"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_block_status database.py")


async def list_block_words(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            group_name = group_info["group_name"]
            words_blocked = group_info["blocked_words"]["blocked_list"]
            if len(words_blocked) > 0:
                lists = [f"▫️ {i['word']}" for i in words_blocked]
                all_words = "\n".join(lists)
                blocked_message = f"📜 --الكلمات المحظورة : {group_name}--\n\n{all_words}\n-"
                return blocked_message
            else:
                return "**❌ لا يوجد كلمات محظورة**"
        else:
            return "**❌ لا يوجد كلمات محظورة**"
    except Exception as error:
        await send_error(client, error, "list_block_words database.py")


async def add_block_words(client, group_id, word):
    try:
        group_info = groups.find_one({"group_id": int(group_id),
                                      'blocked_words.blocked_list': {
                                          "word": word
                                      }})
        if group_info:
            return "NOT_DONE"
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$push": {"blocked_words.blocked_list": {"word": word}}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "add_block_words database.py")


async def remove_block_words(client, group_id, word):
    try:
        group_info = groups.find_one({"group_id": int(group_id),
                                      'blocked_words.blocked_list': {
                                          "word": word
                                      }})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$pull": {"blocked_words.blocked_list": {"word": word}}}
            groups.update_one(state1, state2)
            return f"**✅ تم الغاء حظر الكلمة : {word}**"
        else:
            return "**❌ الكلمة ليست محظورة**"
    except Exception as error:
        await send_error(client, error, "remove_block_words database.py")


async def remove_all_block_words(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            words_blocked = group_info["blocked_words"]["blocked_list"]
            if len(words_blocked) > 0:
                state1 = {"group_id": int(group_id)}
                state2 = {"$set": {"blocked_words.blocked_list": []}}
                groups.update_one(state1, state2)
                return "**✅ تم ازالة جميع الكلمات المحظورة**"
            else:
                return "**❌ لا يوجد كلمات محظورة**"
        else:
            return "**❌ لا يوجد كلمات محظورة**"
    except Exception as error:
        await send_error(client, error, "remove_all_block_words database.py")


async def delete_admin_message(client, message_id):
    try:
        message = messageFromAdmin.find_one({
            "message_id": int(message_id)
        })
        if message:
            the_message = message["message"]
            await client.delete_messages(message['chat_id'], message['message_id'])
            messageFromAdmin.delete_one({"message_id": int(message_id)})
            return the_message
        else:
            return "NOT_DONE"
    except Exception as error:
        await send_error(client, error, "delete_admin_message database.py")


async def add_allowed_words(client, group_id, word):
    try:
        group_info = groups.find_one({"group_id": int(group_id),
                                      'white_list.allowed_list': {
                                          "word": word
                                      }})
        if group_info:
            return "NOT_DONE"
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$push": {"white_list.allowed_list": {"word": word}}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "add_allowed_words database.py")


async def change_allow_status(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info["white_list"]["active"]
        if type_state == "YES":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"white_list.active": "NO"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"white_list.active": "YES"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_allow_status database.py")


async def list_allow_words(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            group_name = group_info["group_name"]
            words_allowed = group_info["white_list"]["allowed_list"]
            if len(words_allowed) > 0:
                lists = [f"▫️ {i['word']}" for i in words_allowed]
                all_words = "\n".join(lists)
                allowed_message = f"📜 --الكلمات المسموح بها : {group_name}--\n\n{all_words}\n-"
                return allowed_message
            else:
                return "**❌ لا يوجد كلمات مسموح بها**"
        else:
            return "**❌ لا يوجد كلمات مسموح بها**"
    except Exception as error:
        await send_error(client, error, "list_allow_words database.py")


async def remove_all_allowed_words(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            words_allowed = group_info["white_list"]["allowed_list"]
            if len(words_allowed) > 0:
                state1 = {"group_id": int(group_id)}
                state2 = {"$set": {"white_list.allowed_list": []}}
                groups.update_one(state1, state2)
                return "**✅ تم ازالة جميع الكلمات المسموح بها**"
            else:
                return "**❌ لا يوجد كلمات مسموح بها**"
        else:
            return "**❌ لا يوجد كلمات محظورة**"
    except Exception as error:
        await send_error(client, error, "remove_all_allowed_words database.py")


async def remove_allowed_words(client, group_id, word):
    try:
        group_info = groups.find_one({"group_id": int(group_id),
                                      'white_list.allowed_list': {
                                          "word": word
                                      }})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$pull": {"white_list.allowed_list": {"word": word}}}
            groups.update_one(state1, state2)
            return f"**✅ تم الغاء السماح بالكلمة : {word}**"
        else:
            return "**❌ الكلمة غير مسموح بها**"
    except Exception as error:
        await send_error(client, error, "remove_allowed_words database.py")


async def update_group_rules(client, group_id, text):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"rules.message": text}}
            groups.update_one(state1, state2)
            return "**✅ تم تحديث القوانين**"
        else:
            return "**⚠️ خطأ في تحديث القوانين**"
    except Exception as error:
        await send_error(client, error, "update_group_rules database.py")


async def change_rules_setting(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        rules_state = group_info['rules']['active']
        if rules_state == "NO":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"rules.active": "YES"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"rules.active": "NO"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_media_setting database.py")


async def change_rules_place(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        rules_state = group_info['rules']['place']
        if rules_state == "GROUP":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"rules.place": "PRIVATE"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"rules.place": "GROUP"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_media_setting database.py")


async def update_group_welcome(client, group_id, text):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"welcome.message": text}}
            groups.update_one(state1, state2)
            return "**✅ تم تحديث رسالة الترحيب**"
        else:
            return "**⚠️ خطأ في تحديث رسالة الترحيب**"
    except Exception as error:
        await send_error(client, error, "update_group_welcome database.py")


async def change_welcome_setting(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        rules_state = group_info['welcome']['active']
        if rules_state == "NO":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"welcome.active": "YES"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"welcome.active": "NO"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_welcome_setting database.py")


async def change_silent_setting(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        rules_state = group_info['silent']['active']
        if rules_state == "NO":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"silent.active": "YES"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"silent.active": "NO"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_silent_setting database.py")


async def check_lang_status(client, group_id, dest):
    try:
        group_info = groups.find_one({"group_id": int(group_id), f"language_settings.{dest}": "❌"})
        if group_info:
            return "NOT_ALLOWED"
        else:
            return "ALLOWED"
    except Exception as error:
        await send_error(client, error, "check_lang_status database.py")


async def check_blocked_status(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            status = group_info['blocked_words']['active']
            if status == "NO":
                return "NOT_ACTIVE"
            else:
                return "ACTIVE"
    except Exception as error:
        await send_error(client, error, "check_blocked_status database.py")


async def check_blocked_word(client, group_id, word):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            list_blocked = group_info['blocked_words']['blocked_list']
            for i in list_blocked:
                blocked = i['word']
                if blocked in word:
                    return "BLOCKED"
                    break
    except Exception as error:
        await send_error(client, error, "check_blocked_word database.py")


async def check_allowed_status(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            status = group_info['white_list']['active']
            if status == "NO":
                return "NOT_ACTIVE"
            else:
                return "ACTIVE"
    except Exception as error:
        await send_error(client, error, "check_allowed_status database.py")


# async def check_allowed_word(client, group_id, word):
#     try:
#         group_info = groups.find_one({"group_id": int(group_id)})
#         if group_info:
#             list_blocked = group_info['white_list']['allowed_list']
#             for i in list_blocked:
#                 blocked = i['word']
#                 if blocked in word:
#                     return "ALLOWED"
#                     break
#     except Exception as error:
#         await send_error(client, error, "check_allowed_word database.py")


def get_groups():
    try:
        count_groups = groups.count_documents({})
        if count_groups > 0:
            list_groups = groups.find()
            return list_groups
        else:
            return "NO_GROUPS"
    except Exception as error:
        print(error)
        # we can not use it now because we use it in main file so we use print :(
        # await send_error(client, error, "get_groups database.py")


async def set_timer_lock(client, group_id, time):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"silent.total_time": time}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_timer_lock database.py")


async def set_timer_unlock(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"silent.total_time": "None"}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_timer_lock database.py")


async def set_from_time_lock(client, group_id, time):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"silent.from_time": time}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_timer_lock database.py")


async def set_from_time_unlock(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"silent.from_time": "None"}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_timer_lock database.py")


async def set_to_time_lock(client, group_id, time):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"silent.to_time": time}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_timer_lock database.py")


async def set_to_time_unlock(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {"silent.to_time": "None"}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_timer_lock database.py")


async def check_rules(client, group_id):
    try:
        is_group = groups.find_one({"group_id": int(group_id)})
        if is_group:
            rules = is_group['rules']
            return rules
        else:
            return "NO"
    except Exception as error:
        await send_error(client, error, "check_rules database.py")


async def change_permission_setting(client, group_id, permission_type):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info['permission'][permission_type]
        if type_state == "✅":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"permission.{permission_type}": "❌"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"permission.{permission_type}": "✅"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_permission_setting database.py")


async def get_permission_group(client, group_id, permission_type):
    """
       الحصول على معلومات اعدادات القفل
    """
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            return group_info['permission'][permission_type]
        else:
            return "NO_GROUP"
    except Exception as error:
        await send_error(client, error, "get_permission_group database.py")



async def change_lock_setting(client, group_id, lock_type):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        type_state = group_info['permission'][lock_type]
        if type_state == "✅":
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"permission.{lock_type}": "❌"}}
            groups.update_one(state1, state2)
        else:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"permission.{lock_type}": "✅"}}
            groups.update_one(state1, state2)
        return "DONE"
    except Exception as error:
        await send_error(client, error, "change_lock_setting database.py")



async def set_message_lock(client, group_id, message, tayp_message):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": {f"silent.{tayp_message}": message}}
            groups.update_one(state1, state2)
            return "DONE"
    except Exception as error:
        await send_error(client, error, "set_message_lock database.py")


async def get_message_lock(client, group_id, type_message):
    """
        جلب رسالة القفل من قواعد البيانات
        - type_message = [lock_message or unlock_message or timer_lock_message]
        - lock_message = رسالة القفل اليومي
        - unlock_message = رسالة الفتح اليومي
        - timer_lock_message = رسالة القفل المؤقت
        - ad_lock_message = الرسالة الإعلانية بعد القفل اليومي
    """
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            return group_info["silent"][type_message]

    except Exception as error:
        await send_error(client, error, "get_message_lock database.py")


async def update_database(client, group_id):
    try:
        group_info = groups.find_one({"group_id": int(group_id)})
        if group_info:
            group_name = group_info['group_name']
            group_id = group_info['group_id']
            group_type = group_info['group_type']
            user_name = group_info['user_name']
            user_id = group_info['user_id']
            control_panel_place = group_info['control_panel_place']
            captcha = group_info['captcha']
            white_list = group_info['white_list']
            blocked_words = group_info['blocked_words']
            silent = group_info['silent']
            warn = group_info['warn']
            welcome = group_info['welcome']
            flood = group_info['flood']
            rules = group_info['rules']
            permissions = group_info['permissions']
            media_settings = group_info['media_settings']
            language_settings = group_info['language_settings']

            # اعدادات المجموعة
            group_info = {
                'group_name': group_name,
                'group_id': group_id,
                'group_type': group_type,
                'user_name': user_name,
                'user_id': user_id,
                'control_panel_place': control_panel_place,
                'captcha': captcha,
                'media_settings': media_settings,
                'language_settings': language_settings,
                'white_list': white_list,
                'blocked_words': blocked_words,
                'silent': silent,
                'warn': warn,
                'flood': flood,
                'welcome': welcome,
                'rules': rules,
                'permission': permissions
                # هنا يتم اضافة المعلومات الجديدة
            }
            state1 = {"group_id": int(group_id)}
            state2 = {"$set": group_info}
            groups.update_one(
                state1,
                state2
            )
            return "**✅ تم تحديث قواعد البيانات**"
        else:
            return "**⚠️ المجموعة غير مفعلة**"
    except Exception as error:
        await send_error(client, error, "update_database database.py")



async def set_block_user(client,first_name,last_name,user_name,user_id):
    """
    حظر اليوزر من مراسلة القروب
    """
    try:
        now = datetime.now()  # الوقت / التاريخ
        block_date = now.strftime("%Y-%m-%d %H:%M:%S")  # تاريخ الحظر

        user_info = {
            "first_name": first_name,
            "last_name": last_name,
            "user_name": user_name,
            "user_id": user_id,
            "block_date": block_date
        }
        is_user = block_user.find_one({"user_id": int(user_id)})
        if not is_user:
            block_user.insert_one(user_info)
    except Exception as error:
        await send_error(client, error, "set_block_user database.py")

async def user_block_lists(client):
    """
        قائمة المحظورين
    """
    try:
        count_block_user = block_user.count_documents({})
        if count_block_user > 0:
            list_block_user = block_user.find()
            keyboard_list = []
            
            keyboard = InlineKeyboardMarkup(
                keyboard_list,
            )
            for i in list_block_user:
                keyboard_list.append(
                    [
                        InlineKeyboardButton(
                            text=f"{i['first_name']} = {i['user_name']}",
                            callback_data=f"block_user_settings#{i['user_id']}"
                        )
                    ]
                )
            return keyboard
        else:
            return "NO_USER_BLOCK"
    except Exception as error:
        await send_error(client, error, "user_block_lists database.py")


async def check_block_user(client, user_id):
    try:
        user_info = block_user.find_one({"user_id": int(user_id)})
        if user_info:
            return "BLOCKED"
        else:
            return "ALLOWED"
    except Exception as error:
        await send_error(client, error, "check_block_user database.py")


async def remove_all_block_user(client):
    try:
        if block_user:
            block_user.drop()
            return "**✅ تم ازالة جميع المحظورين**"
        else:
            return "**❌ لا يوجد محظورين**"
    except Exception as error:
        await send_error(client, error, "remove_all_block_user database.py")


async def set_message_from_private(client, message, message_id):
    """
        messageFromPrivate
    """
    try:
        first_name = message.from_user.first_name  # الاسم الاول
        last_name = message.from_user.last_name if message.from_user.last_name else "❌"  # الاسم الثاني
        user_name = message.from_user.username if message.from_user.username else "❌"  # المعرف
        user_id = message.from_user.id  # الايدي
        text = message.text # نص الرسالة

        user_info = {
            "first_name": first_name,
            "last_name": last_name,
            "user_name": user_name,
            "user_id": user_id,
            "message_id": message_id,
            "text": text,
        }
        
        messageFromPrivate.insert_one(user_info)
    except Exception as error:
        await send_error(client, error, "set_message_from_private database.py")



async def check_message_from_private(client, message_id):
    try:
        message_info = messageFromPrivate.find_one({"message_id": int(message_id)})
        if message_info:
            return message_info
        
    except Exception as error:
        await send_error(client, error, "check_message_from_private database.py")