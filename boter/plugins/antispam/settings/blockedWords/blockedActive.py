from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, change_block_status
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("blockedActive"))
async def blocked_active_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("blockedActive#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            change_status = await change_block_status(client, data)
            if change_status == "DONE":
                group_data = await get_group_settings(client, data)
                group_name = group_data['group_name']
                blocked_status = group_data['blocked_words']['active']
                group_message = f"🚫--اعدادات الكلمات المحظورة :-- **{group_name}**\n-"
                banded_active = "❌" if blocked_status == "NO" else "✅"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                f"♻️ حالة الكلمات :  {banded_active}",
                                callback_data=f"blockedActive#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "📃 الكلمات المحظورة",
                                callback_data=f"listBlockedWords#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "➕",
                                callback_data=f"addBlockedWord#{data}"
                            ),
                            InlineKeyboardButton(
                                "➖",
                                callback_data=f"removeBlockedWord#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "⚠️ ازالة جميع الكلمات",
                                callback_data=f"removeAllBlocked#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"groups_settings#{data}"
                            )
                        ],
                    ]
                )
                await callback_query.answer(banded_active)
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "blocked_active_settings blockedActive.py")
