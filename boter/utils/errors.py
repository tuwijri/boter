from boter.config import CHANNEL_LOG
import traceback


async def send_error(client, error, path):
    """
    - send_error: ارسال الاخطاء لقناة
        > client : لارسال الرسائل بإستخدام دالة send_message
        > error : تحديد الخطأ
        > path : مسار الخطأ , الملف واسم الدالة
    """
    try:
        my_traceback = traceback.format_exc()
        await client.send_message(CHANNEL_LOG, f"**⚠️ Error:**\n**▪️Function : ** `{path}`\n\n```{error}```\n\n**🪲traceback : **```{my_traceback}```")
    except Exception as code_error:
        await client.send_message(CHANNEL_LOG,
                                  f"**⚠️ Error:**\n**▪️Function : ** `send_error errors`\n\n```{code_error}```")
