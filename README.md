

# Configuration

Environment vairables are passed to the run command for configuring a boter container.






Name                | Default         | Description
--------------------|-----------------|------------
API_ID              | ''              | you have to get you api_id from [here](https://my.telegram.org)
API_HASH            | ''              | you have to get you api_hash from [here](https://my.telegram.org)
BOT_TOKEN           | ''              | you have to get token your bot example: [here](https://core.telegram.org/bots#6-botfather)
BOT_USERNAME        | ''              | username your bot 'example_bot' without '@'
CHANNEL_LOG         | ''              | The ID of the group or the channel on which the error log will be received
ADMINS              | ''              | list ID admin bot example: [12345,32154,147147]
DATABASE_HOST       | 'mongo'         | MongoDB container name or URL host mongodb
DATABASE_PORT       |  27017          | MongoDB port
DATABASE_NAME       | 'boter_data'    | The name of the data rules you want to create
DATABASE_USERNAME   | ''              | MongoDB admin username , Make it blank if you don't have User Name and Password for your databases
DATABASE_PASSWORD   | ''              | MongoDB admin password , Make it blank if you don't have User Name and Password for your databases

