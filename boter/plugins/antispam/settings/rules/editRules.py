from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, update_group_rules
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("editRules"))
async def edit_rules_settings(client, callback_query):
    try:
        button_data = callback_query.data
        user_id = callback_query.from_user.id
        message = callback_query.message
        data = button_data.split("editRules#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_message = "⏳ أرسل القوانين .."
            bot_reply = await callback_query.edit_message_text(f"{group_message}\n- أرسل الامر : /cancel للإلغاء\n-")
            reply = await message.chat.listen(filters.text & filters.user(user_id))
            text = reply.text
            bot_username = await client.get_me()
            cancel_word = [
                "/cancel", f"/cancel@{bot_username.username}",
                "#cancel", f"#cancel@{bot_username.username}",
                "#الغاء", f"#الغاء@{bot_username.username}",
                "/الغاء", f"/الغاء@{bot_username.username}",
                ""
            ]
            if text in cancel_word:
                message.chat.cancel_listener()
                await bot_reply.delete()
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"rules_settings_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply('**✅ تم الإلغاء**', reply_markup=keyboard)
            else:
                await bot_reply.delete()
                update_rules = await update_group_rules(client, data, text)
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"rules_settings_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply(f"{update_rules}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "edit_rules_settings editRules.py")
