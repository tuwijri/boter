from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, change_rules_place
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("rulesPlace"))
async def rules_place_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("rulesPlace#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            change_rules = await change_rules_place(client, data)
            if change_rules == "DONE":
                group_data = await get_group_settings(client, data)
                group_name = group_data['group_name']
                rules_status = group_data['rules']['active']
                rules_place = group_data['rules']['place']
                group_message = f"🚫--قوانين المجموعة :-- **{group_name}**\n-"
                rules_active = "❌" if rules_status == "NO" else "✅"
                rules_place_status = "👥 المجموعة" if rules_place == "GROUP" else "👤 الخاص"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "📓 عرض القوانين",
                                callback_data=f"showRules#{data}"
                            ),
                            InlineKeyboardButton(
                                "✏️ تعديل القوانين",
                                callback_data=f"editRules#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                f"📌 مكان القوانين : {rules_place_status}",
                                callback_data=f"rulesPlace#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                f"♻️ حالة القوانين : {rules_active}",
                                callback_data=f"rulesActive#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"groups_settings#{data}"
                            )
                        ],
                    ]
                )
                await callback_query.answer(f"{rules_place_status}")
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
            else:
                await callback_query.answer("⚠️")
    except Exception as error:
        await send_error(client, error, "rules_place_settings rulesPlace.py")
