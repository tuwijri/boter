from pyrogram import filters
from .database import check_group_admins, check_admin, get_media_state, check_block_user
from boter.config import ADMINS, BOT_TOKEN


def check_bot_admin(self, client, message):
    """
     فلتر للتأكد اذا كان العضو مشرف في البوت أو لا
    """
    user_id = message.from_user.id
    is_admin = check_admin(client, user_id)
    if is_admin == "ADMIN" or user_id in ADMINS:
        return True
    else:
        return False


IS_ADMIN = filters.create(check_bot_admin)


def check_command_and_hash(self, client, message):
    """
     فلتر للتأكد هل الرسالة فيها امر كوماند او هاش
    """
    if message.entities:
        if message.entities[0].type in ['bot_command', 'hashtag']:
            return True
    elif message.caption_entities:
        if message.caption_entities[0].type in ['bot_command', 'hashtag']:
            return True
    else:
        return False


CHECK_COMMAND = filters.create(check_command_and_hash)

def check_mobile(self, client, message):
    """
     فلتر للتأكد هل الرسالة فيها رقم جوال
    """
    group_id = message.chat.id
    mobile = get_media_state(client, group_id, "mobile")
    if mobile == "❌":
        if message.entities:
            if message.entities[0].type in ['phone_number']:
                return True
        elif message.caption_entities:
            if message.caption_entities[0].type in ['phone_number']:
                return True
        else:
            return False
    else:
        return False


CHECK_MOBILE = filters.create(check_mobile)

def check_link(self, client, message):
    """
     فلتر للتأكد هل الرسالة فيها رابط
    """
    group_id = message.chat.id
    chat_id = message.chat.id
    link = get_media_state(client, group_id, "link")
    telegram_link = get_media_state(client, chat_id, "telegram_link")
    if link == "❌" or telegram_link == "❌":
        if message.entities:
            if message.entities[0].type in ['url','text_link']:
                return True
        elif message.caption_entities:
            if message.caption_entities[0].type in ['url','text_link']:
                return True
        else:
            return False
    else:
        return False


CHECK_LINK = filters.create(check_link)




async def check_user_block(self, client, message):
    """
     فلتر للتأكد هل اليوزر محظور او لا
    """
    user_id = message.chat.id
    blocked = await check_block_user(client,user_id)
    if blocked == "BLOCKED":
        return False
    else:
        return True


CHECK_USER_BLOCKED = filters.create(check_user_block)

def check_message_from_group_admins(self, client, message):
    """
     فلتر للتاكد من ان الرسالة من قروب الادمن
    """
    is_group = check_group_admins(client)
    if is_group != "NO_GROUP" and is_group == message.chat.id:
        return True
    else:
        return False


GROUP_ADMINS = filters.create(check_message_from_group_admins)


def check_message_from_bot(self, client, message):
    """
     التحقق من ان الرد على نفس البوت
    """
    if message.reply_to_message is not None:
        BOT_ID = int(BOT_TOKEN.split(":")[0])
        if message.reply_to_message.from_user.id == BOT_ID and message.reply_to_message.forward_date:
            return True
        else:
            return False
    else:
        return False


FROM_BOT = filters.create(check_message_from_bot)


def check_audio_status(self, client, message):
    """
     التحقق من حالة الصوت
    """
    group_id = message.chat.id
    audio = get_media_state(client, group_id, "audio")
    if audio == "❌":
        return True
    else:
        return False


AUDIO_FILTER = filters.create(check_audio_status)


def check_document_status(self, client, message):
    """
     التحقق من حالة الملفات
    """
    group_id = message.chat.id
    document = get_media_state(client, group_id, "document")
    if document == "❌":
        return True
    else:
        return False


DOCUMENT_FILTER = filters.create(check_document_status)


def check_forward_status(self, client, message):
    """
     التحقق من اعادة التوجيه
    """
    group_id = message.chat.id
    forward = get_media_state(client, group_id, "forward")
    if forward == "❌":
        return True
    else:
        return False


FORWARD_FILTER = filters.create(check_forward_status)


def check_games_status(self, client, message):
    """
     التحقق من الالعاب
    """
    group_id = message.chat.id
    games = get_media_state(client, group_id, "games")
    if games == "❌":
        return True
    else:
        return False


GAMES_FILTER = filters.create(check_games_status)


def check_gif_status(self, client, message):
    """
     التحقق من الصور المتحركة
    """
    group_id = message.chat.id
    gif = get_media_state(client, group_id, "gif")
    if gif == "❌":
        return True
    else:
        return False


GIF_FILTER = filters.create(check_gif_status)


def check_location_status(self, client, message):
    """
     التحقق من الموقع
    """
    group_id = message.chat.id
    location = get_media_state(client, group_id, "location")
    if location == "❌":
        return True
    else:
        return False


LOCATION_FILTER = filters.create(check_location_status)


def check_photo_status(self, client, message):
    """
     التحقق من الصور
    """
    group_id = message.chat.id
    photo = get_media_state(client, group_id, "photo")
    if photo == "❌":
        return True
    else:
        return False


PHOTO_FILTER = filters.create(check_photo_status)


def check_sticker_status(self, client, message):
    """
     التحقق من الملصقات
    """
    group_id = message.chat.id
    sticker = get_media_state(client, group_id, "sticker")
    if sticker == "❌":
        return True
    else:
        return False


STICKER_FILTER = filters.create(check_sticker_status)


def check_video_status(self, client, message):
    """
     التحقق من الفيديو
    """
    group_id = message.chat.id
    video = get_media_state(client, group_id, "video")
    if video == "❌":
        return True
    else:
        return False


VIDEO_FILTER = filters.create(check_video_status)


def check_video_note_status(self, client, message):
    """
     التحقق من ملاحظات الفيديو
    """
    group_id = message.chat.id
    video_note = get_media_state(client, group_id, "video_note")
    if video_note == "❌":
        return True
    else:
        return False


VIDEO_NOTE_FILTER = filters.create(check_video_note_status)


def check_voice_status(self, client, message):
    """
     التحقق من الصوت
    """
    group_id = message.chat.id
    voice = get_media_state(client, group_id, "voice")
    if voice == "❌":
        return True
    else:
        return False


VOICE_FILTER = filters.create(check_voice_status)
