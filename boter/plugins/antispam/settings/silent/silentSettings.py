from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, get_permission_group, change_permission_setting
from boter.plugins.antispam.settings.permissions.PermissionsGroup import close_group, open_group
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("silent_settings"))
async def silent_settings_(client, callback_query):
    try:
        button_data = callback_query.data
        group_id = button_data.split("#")[1]
        active = button_data.split("#")[2]
        lock_status = await client.get_chat(group_id)
        if active == "active":
            if lock_status.permissions.can_send_messages:
                await close_group(client, int(group_id))
            elif not lock_status.permissions.can_send_messages:
                await open_group(client, int(group_id))
    except:
        pass
    try:
        button_data = callback_query.data
        group_id = button_data.split("#")[1]
        group_data = await get_group_settings(client, int(group_id))
        is_group = await check_group(client, int(group_id))
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"👥--اعدادات قفل المجموعة :-- **{group_name}**\n-"
            lock_status = await client.get_chat(int(group_id))
            silent_status = "🔔" if lock_status.permissions.can_send_messages == True else "🔕"
            keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                f"🔘 حالة القفل : {silent_status}",
                                callback_data=f"edit_silent_settings#{group_id}#active"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "📆 قفل يومي",
                                callback_data=f"EveryDay#{group_id}"
                            ),
                            InlineKeyboardButton(
                                "⏰ قفل مؤقت",
                                callback_data=f"TimerLock#{group_id}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🏷 ضبط الصلاحيات",
                                callback_data=f"LockGroupList#{group_id}#New"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "📨 ضبط رسائل القفل",
                                callback_data=f"messageLock#{group_id}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"groups_settings#{group_id}"
                            )
                        ],
                    ]
                )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
            await callback_query.answer(f"{silent_status}")

    except Exception as error:
        await send_error(client,error, "silent_settings silentSettings.py")


@Boter.on_callback_query(filters.regex("LockGroupList"))
async def Lock_group_list(client, callback_query):
    try:       
        button_data = callback_query.data
        group_id = button_data.split("#")[1]
        is_group = await check_group(client, group_id)
        if is_group != "NO_GROUP":
            if button_data.split("#")[2] != "New":
                permission_type = button_data.split("#")[2]
                change = await change_permission_setting(client, group_id, permission_type)


            can_send_messages = await get_permission_group(client, group_id, 'can_send_messages')
            can_send_media_messages = await get_permission_group(client, group_id, 'can_send_media_messages')
            can_send_other_messages = await get_permission_group(client, group_id, 'can_send_other_messages')
            can_send_polls = await get_permission_group(client, group_id, 'can_send_polls')
            can_add_web_page_previews = await get_permission_group(client, group_id, 'can_add_web_page_previews')
            can_change_info = await get_permission_group(client, group_id, 'can_change_info')
            can_invite_users = await get_permission_group(client, group_id, 'can_invite_users')
            can_pin_messages = await get_permission_group(client, group_id, 'can_pin_messages')

            group_data = await get_group_settings(client, group_id)
            group_name = group_data['group_name']
            group_message = f"🧾 --اعدادات الصلاحيات :-- **{group_name}**\n\n✅ **: مسموح به**\n❌ **: ممنوع**\n-"

            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "✉️ إرسال الرسائل"+"  "+can_send_messages,
                            callback_data=f"LockGroupList#{group_id}#can_send_messages"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🎆 إرسال الوسائط"+"  "+can_send_media_messages,
                            callback_data=f"LockGroupList#{group_id}#can_send_media_messages"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🖼 إرسال الملصقات والصور المتحركة"+"  "+can_send_other_messages,
                            callback_data=f"LockGroupList#{group_id}#can_send_other_messages"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "📊 إرسال الاستفتاءات"+"  "+can_send_polls,
                            callback_data=f"LockGroupList#{group_id}#can_send_polls"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔍 معاينة الروابط"+"  "+can_add_web_page_previews,
                            callback_data=f"LockGroupList#{group_id}#can_add_web_page_previews"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "📝 تغيير معلومات المجموعة"+"  "+can_change_info,
                            callback_data=f"LockGroupList#{group_id}#can_change_info"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "👥 إضافة الأعضاء"+"  "+can_invite_users,
                            callback_data=f"LockGroupList#{group_id}#can_invite_users"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "📌 تثبيت الرسائل"+"  "+can_pin_messages,
                            callback_data=f"LockGroupList#{group_id}#can_pin_messages"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"silent_settings#{group_id}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "media_settings Lock_group_list.py")