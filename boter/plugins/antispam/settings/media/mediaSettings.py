from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import get_media_settings, check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("media_setting_"))
async def media_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("media_setting_")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            document = await get_media_settings(client, data, 'document')
            photo = await get_media_settings(client, data, 'photo')
            video = await get_media_settings(client, data, 'video')
            voice = await get_media_settings(client, data, 'voice')
            audio = await get_media_settings(client, data, 'audio')
            sticker = await get_media_settings(client, data, 'sticker')
            video_note = await get_media_settings(client, data, 'video_note')
            gif = await get_media_settings(client, data, 'gif')
            forward = await get_media_settings(client, data, 'forward')
            telegram_link = await get_media_settings(client, data, 'telegram_link')
            link = await get_media_settings(client, data, 'link')
            mobile = await get_media_settings(client, data, 'mobile')
            tag = await get_media_settings(client, data, 'tag')
            hashtag = await get_media_settings(client, data, 'hashtag')
            bots = await get_media_settings(client, data, 'bots')
            join_service = await get_media_settings(client, data, 'join_service')
            left_service = await get_media_settings(client, data, 'left_service')
            location = await get_media_settings(client, data, 'location')
            games = await get_media_settings(client, data, 'games')

            group_data = await get_group_settings(client, data)
            group_name = group_data['group_name']
            group_message = f"🌌 --اعدادات الوسائط :-- **{group_name}**\n\n✅ **: مسموح به**\n❌ **: ممنوع**\n-"
            print(1111)
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            document,
                            callback_data=f"group_media#document#{data}"
                        ),
                        InlineKeyboardButton(
                            "🗂 الملفات",
                            callback_data=b"Documents"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            photo,
                            callback_data=f"group_media#photo#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎆 الصور",
                            callback_data=b"Photos"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            video,
                            callback_data=f"group_media#video#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎥 الفيديو",
                            callback_data=b"Videos"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            voice,
                            callback_data=f"group_media#voice#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎙 تسجيلات الصوت",
                            callback_data=b"Voices"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            audio,
                            callback_data=f"group_media#audio#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎶 الموسيقى",
                            callback_data=b"Audios"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            sticker,
                            callback_data=f"group_media#sticker#{data}"
                        ),
                        InlineKeyboardButton(
                            "🌠 الملصقات",
                            callback_data=b"Stickers"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            video_note,
                            callback_data=f"group_media#video_note#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎥 ملاحظات الفيديو",
                            callback_data=b"VideoNotes"
                        ),

                    ],
                    [
                        InlineKeyboardButton(
                            gif,
                            callback_data=f"group_media#gif#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎭 الصور المتحركة",
                            callback_data=b"Gifs"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            forward,
                            callback_data=f"group_media#forward#{data}"
                        ),
                        InlineKeyboardButton(
                            "🔄 اعادة التوجيه",
                            callback_data=b"Forward"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            telegram_link,
                            callback_data=f"group_media#link#{data}"
                        ),
                        InlineKeyboardButton(
                            "📣 روابط تيليجرام",
                            callback_data=b"Links"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            tag,
                            callback_data=f"group_media#tag#{data}"
                        ),
                        InlineKeyboardButton(
                            "📍 التاقات",
                            callback_data=b"Tag"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            hashtag,
                            callback_data=f"group_media#hashtag#{data}"
                        ),
                        InlineKeyboardButton(
                            "#️⃣ الهاشتاق",
                            callback_data=b"Hashtag"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            link,
                            callback_data=f"group_media#link#{data}"
                        ),
                        InlineKeyboardButton(
                            "🔗 الروابط",
                            callback_data=b"Link"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            mobile,
                            callback_data=f"group_media#mobile#{data}"
                        ),
                        InlineKeyboardButton(
                            "📱 ارقام الجوال",
                            callback_data=b"mobile"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            bots,
                            callback_data=f"group_media#bots#{data}"
                        ),
                        InlineKeyboardButton(
                            "🤖 بوتات",
                            callback_data=b"Bots"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            join_service,
                            callback_data=f"group_media#join_service#{data}"
                        ),
                        InlineKeyboardButton(
                            "🔻اشعارات الدخول",
                            callback_data=b"JoinService"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            left_service,
                            callback_data=f"group_media#left_service#{data}"
                        ),
                        InlineKeyboardButton(
                            "🔺اشعارات الخروج",
                            callback_data=b"LeftService"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            location,
                            callback_data=f"group_media#location#{data}"
                        ),
                        InlineKeyboardButton(
                            "🗺 المواقع",
                            callback_data=b"Location"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            games,
                            callback_data=f"group_media#games#{data}"
                        ),
                        InlineKeyboardButton(
                            "🎮 العاب",
                            callback_data=b"Games"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"groups_settings#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "media_settings mediaSettings.py")
