from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, list_block_words
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("listBlockedWords"))
async def blocked_list(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("listBlockedWords#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            lists = await list_block_words(client, data)
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"blocked_words_{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{lists}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "blocked_list listBlockedWords.py")
