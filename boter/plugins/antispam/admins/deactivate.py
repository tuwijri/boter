from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.config import CMD
from boter.utils.errors import send_error
from boter.utils.database import deactivate_group, get_group_settings, check_group
from boter.utils.filters import IS_ADMIN


@Boter.on_callback_query(filters.regex("accept_deactivate_groups"))
async def accept_deactivate_groups(client, callback_query):
    try:
        message = callback_query.message
        button_data = callback_query.data
        data = button_data.split("accept_deactivate_groups#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"👥--هل انت متأكد من الغاء إدارة المجموعة :-- **{group_name}**\n-"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "نعم",
                            callback_data=f"yes_deactivate_groups#{data}"
                        ),
                        InlineKeyboardButton(
                            "لا",
                            callback_data=f"groups_settings#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"groups_settings#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "accept_deactivate_groups deactivate.py")


@Boter.on_callback_query(filters.regex("yes_deactivate_groups"))
async def deactivate_groups(client, callback_query):
    message = callback_query.message
    button_data = callback_query.data
    group_id = button_data.split("#")[1]
    try:
        deactivate = await deactivate_group(client, group_id)
        await callback_query.edit_message_text(deactivate)
    except Exception as error:
        await send_error(client, error, "deactivate_groups deactivate.py")
