from pyrogram import filters
from boter.utils.errors import send_error
from boter.utils.database import set_block_user, check_message_from_private
from boter.utils.filters import GROUP_ADMINS
from boter.boter import Boter
from boter.config import CMD



@Boter.on_message(filters.command(['حظر','block'], prefixes=CMD) & GROUP_ADMINS)
async def block_user(client, message):
    try:
        if message.reply_to_message.forward_from:
            first_name = message.reply_to_message.forward_from.first_name  # الاسم الاول
            last_name = message.reply_to_message.forward_from.last_name if message.reply_to_message.forward_from.last_name else "❌"  # الاسم الثاني
            user_name = message.reply_to_message.forward_from.username if message.reply_to_message.forward_from.username else "❌"  # المعرف
            user_id = message.reply_to_message.forward_from.id  # الايدي
            await set_block_user(client,first_name,last_name,user_name,user_id)
            await message.reply(f"تم حظر المستخدم:\nID: {user_id}\nUser Name: @{user_name}")
        else:
            message_id = message.reply_to_message.message.id
            user_info = await check_message_from_private(client, message_id)
            first_name = user_info['first_name']
            last_name = user_info['last_name']
            user_name = user_info['user_name']
            user_id = user_info['user_id']
            await set_block_user(client,first_name,last_name,user_name,user_id)
            await message.reply(f"تم حظر المستخدم:\nID: {user_id}\nUser Name: @{user_name}")

    except Exception as error:
        await send_error(client, error, "block_user block.py")
