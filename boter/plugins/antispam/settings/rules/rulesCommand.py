from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from pyrogram.errors import UserNotParticipant
from boter.boter import Boter
from boter.config import CMD, BOT_USERNAME
from boter.utils.database import check_group, check_rules, check_group_admins
from boter.utils.errors import send_error


@Boter.on_message(filters.command(['القوانين'], prefixes=CMD) & filters.group)
async def rules_command(client, message):
    try:
        chat_id = message.chat.id
        user_id = message.from_user.id
        is_group = await check_group(client, chat_id)
        if is_group != "NO_GROUP":
            is_rules = await check_rules(client, chat_id)
            active_rules = is_rules['active']
            if active_rules != "NO":
                rules_message = is_rules['message']
                rules_place = is_rules['place']
                group_admins = check_group_admins(client)
                admins = await client.get_chat_member(chat_id, user_id)
                if group_admins != "NO_GROUP":
                    try:
                        group_admins_users = await client.get_chat_member(group_admins, user_id)
                        user_group = group_admins_users.user.id
                        if user_id != user_group or admins.status in ['creator', 'administrator']:
                            if rules_message != "NO_MESSAGE":
                                if rules_place == "GROUP":
                                    await message.reply(rules_message)
                                else:
                                    await message.reply("**▪️اضغط الزر لقراءة القوانين :**",
                                                        reply_markup=InlineKeyboardMarkup(
                                                            [
                                                                [
                                                                    InlineKeyboardButton(
                                                                        "📄 قوانين المجموعة",
                                                                        url=f"https://t.me/{BOT_USERNAME}?start=rules_{chat_id}"
                                                                    )
                                                                ],
                                                            ]
                                                        )
                                                        )
                            else:
                                await message.reply("**❌ لا يوجد قوانين **")
                    except UserNotParticipant:
                        if admins.status in ['creator', 'administrator']:
                            if rules_message != "NO_MESSAGE":
                                if rules_place == "GROUP":
                                    await message.reply(rules_message)
                                else:
                                    await message.reply("**▪️اضغط الزر لقراءة القوانين :**",
                                                        reply_markup=InlineKeyboardMarkup(
                                                            [
                                                                [
                                                                    InlineKeyboardButton(
                                                                        "📄 قوانين المجموعة",
                                                                        url=f"https://t.me/{BOT_USERNAME}?start=rules_{chat_id}"
                                                                    )
                                                                ],
                                                            ]
                                                        )
                                                        )
                            else:
                                await message.reply("**❌ لا يوجد قوانين **")
                else:
                    if admins.status in ['creator', 'administrator']:
                        if rules_message != "NO_MESSAGE":
                            if rules_place == "GROUP":
                                await message.reply(rules_message)
                            else:
                                await message.reply("**▪️اضغط الزر لقراءة القوانين :**",
                                                    reply_markup=InlineKeyboardMarkup(
                                                        [
                                                            [
                                                                InlineKeyboardButton(
                                                                    "📄 قوانين المجموعة",
                                                                    url=f"https://t.me/{BOT_USERNAME}?start=rules_{chat_id}"
                                                                )
                                                            ],
                                                        ]
                                                    )
                                                    )
                        else:
                            await message.reply("**❌ لا يوجد قوانين **")
    except Exception as error:
        await send_error(client, error, "rules_command rulesCommand.py")
