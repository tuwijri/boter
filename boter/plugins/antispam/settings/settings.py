from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.config import CMD, BOT_USERNAME
from boter.utils.database import group_lists, get_group_settings
from boter.utils.errors import send_error
from boter.utils.filters import GROUP_ADMINS


@Boter.on_message(filters.command(["الاعدادات"], prefixes=CMD) & GROUP_ADMINS)
async def settings_lists(client, message):
    """
        اعدادات
    """
    try:
        
        group_message = f"⚙️-الاعدادات\n-"
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text="اعدادات المجموعات",
                        callback_data=f"settings_group_lists"
                    )
                ],
                [
                    InlineKeyboardButton(
                        text="اعدادات البوت ",
                        callback_data=f"settings_bot"
                    )
                ],
            ]
        )
        await message.reply(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "settings_lists settings.py")



@Boter.on_callback_query(filters.regex("Back_settings_bot"))
async def Back_settings_bot(client, callback_query):
    """
        اعدادات
    """
    try:
        
        group_message = f"⚙️-الاعدادات\n-"
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text="اعدادات المجموعات",
                        callback_data=f"settings_group_lists"
                    )
                ],
                [
                    InlineKeyboardButton(
                        text="اعدادات البوت ",
                        callback_data=f"settings_bot"
                    )
                ],
            ]
        )
        await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "Back_settings_bot settings.py")


@Boter.on_callback_query(filters.regex("settings_bot"))
async def settings_bot(client, callback_query):
    """
        اعدادات المجموعات
    """
    try:
        
        group_message = f"⚙️-الاعدادات\n-"
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text="حذف رسالة تم ارسالها",
                        callback_data=f"delete_admins_messages"
                    )
                ],
                [
                    InlineKeyboardButton(
                        text="المحظورين",
                        callback_data=f"blocked_settings"
                    )
                ],
                [
                    InlineKeyboardButton(
                        text="🔙 الرجوع الى الاعدادات الرئيسة",
                        callback_data=f"Back_settings_bot"
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=" ❌ الخروج من الإعدادات",
                        callback_data="exit_settings"
                    )
                ],
               
            ]
        )
        await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "settings settings.py")



@Boter.on_callback_query(filters.regex("settings_group_lists"))
async def choose_group(client, callback_query):

    """
        قائمة المجموعات في البوت
    """
    try:
        message_id = callback_query.message.id
        chat_id = callback_query.message.chat.id
        groups = await group_lists(client, callback_query)       
        if groups == "NO_GROUPS":
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            text=f"➕ اضافة مجموعة جديدة",
                            url=f"http://t.me/{BOT_USERNAME}?startgroup=addGroup_{message_id}_{chat_id}"
                        )
                    ],
                    [
                        InlineKeyboardButton(
                            text="🔙 الرجوع الى الاعدادات الرئيسة",
                            callback_data=f"Back_settings_bot"
                        )
                    ],
                    [
                        InlineKeyboardButton(
                            text=" ❌ الخروج من الإعدادات",
                            callback_data="exit_settings"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text("**❌ لا يوجد مجموعات مفعلة**", reply_markup=keyboard)
        else:
            await callback_query.edit_message_text("**👥 قائمة المجموعات المفعلة : **", reply_markup=groups)
    except Exception as error:
        await send_error(client, error, "choose_group settings.py")

        

@Boter.on_callback_query(filters.regex("groups_settings#"))
async def settings_group(client, callback_query):
    """
        اعدادات المجموعات
    """
    try:
        button_data = callback_query.data
        data = button_data.split("groups_settings#")[1]
        group_data = await get_group_settings(client, data)
        if group_data != "NO_GROUP":
            group_name = group_data['group_name']
            group_id = group_data['group_id']
            group_message = f"⚙️--اعدادات المجموعة :-- **{group_name}**\n-"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            text="🌌 اعدادات الوسائط",
                            callback_data=f"media_setting_{group_id}"
                        ),
                        InlineKeyboardButton(
                            text="⛔️حذف إدارة المجموعات⛔️",
                            callback_data=f"accept_deactivate_groups#{group_id}"                        )
                    ],
                    [
                        InlineKeyboardButton(
                            text="🔕 قفل المجموعة",
                            callback_data=f"silent_settings#{group_id}"
                        ),
                        InlineKeyboardButton(
                            text="🚫 الكلمات المحظورة",
                            callback_data=f"blocked_words_{group_id}"
                        )

                    ],
                    [
                        InlineKeyboardButton(
                            text="🚩 القوانين",
                            callback_data=f"rules_settings_{group_id}"
                        ),
                        InlineKeyboardButton(
                            text="🎊 الترحيب",
                            callback_data=f"welcome_settings_{group_id}"
                        )
                    ],
                    [
                        InlineKeyboardButton(
                            text="🔙 الرجوع الى المجموعات",
                            callback_data=f"backToGroups"
                        )
                    ],
                    [
                        InlineKeyboardButton(
                            text=" ❌ الخروج من الإعدادات",
                            callback_data="exit_settings"
                        )
                    ],

                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "settings_group settings.py")



@Boter.on_callback_query(filters.regex("backToGroups"))
async def group_settings_button(client, callback_query):
    """
        الرجوع الى قائمة المجموعات في البوت
    """
    try:
        groups = await group_lists(client, callback_query)
        if groups == "NO_GROUPS":
            await callback_query.edit_message_text("**❌ لا يوجد مجموعات مفعلة**")
        else:
            await callback_query.edit_message_text("**👥 قائمة المجموعات المفعلة : **", reply_markup=groups)
    except Exception as error:
        await send_error(client, error, "group_settings_button settings.py")
   



@Boter.on_callback_query(filters.regex("exit_settings"))
async def exit_settings(client, callback_query):
    """
       اغلاق  اعدادات المجموعات
    """
    try:
        group_message = "**☑️ تم اغلاق الاعدادات**"
        await callback_query.edit_message_text(f"{group_message}")
    except Exception as error:
        await send_error(client, error, "exit_settings settings.py")
