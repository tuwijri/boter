import html
from pyrogram import Client, filters
from boter.config import CMD
from boter.utils.errors import send_error


@Client.on_message(filters.command(["id"], prefixes=CMD) & filters.private)
async def ids_private(client, message):
    try:
        await message.reply_text("<b>المعلومات:</b>\n\n"
                                 "<b>الإسم:</b> <code>{first_name} {last_name}</code>\n"
                                 "<b>اسم المستخدم:</b> @{username}\n"
                                 "<b>الاي دي الخاص بك:</b> <code>{user_id}</code>\n"
                                 "<b>اللغة:</b> {lang}\n"
                                 "<b>نوع الدردشة:</b> {chat_type}"
                                 "<b> </b>\n\n"
                                 "<b>.</b>\n".format(
                                    first_name=message.from_user.first_name,
                                    last_name=message.from_user.last_name or "",
                                    username=message.from_user.username,
                                    user_id=message.from_user.id,
                                    lang=message.from_user.language_code,
                                    chat_type=message.chat.type
                                    ), parse_mode="HTML")
    except Exception as error:
        await send_error(client, error, "ids_private ids.py")


@Client.on_message(filters.command(["id"], prefixes=CMD) & filters.group)
async def ids(client, message):
    try:
        data = message.reply_to_message or message
        await message.reply_text("<b>معلومات الدردشة:</b>\n\n"
                                 "<b>الإسم:</b> <code>{first_name} {last_name}</code>\n"
                                 "<b>اسم المستخدم:</b> @{username}\n"
                                 "<b>اي دي المستخدم:</b> <code>{user_id}</code>\n"
                                 "<b>مراكز البيانات:</b> {user_dc}\n"
                                 "<b>اللغة:</b> {lang}\n\n"
                                 "<b>اسم القروب:</b> <code>{chat_title}</code>\n"
                                 "<b>اسم المستخدم القروب:</b> @{chat_username}\n"
                                 "<b>اي دي القروب:</b> <code>{chat_id}</code>\n"
                                 "<b>نوع القروب:</b> {chat_type}\n"
                                 "<b> </b>\n\n"
                                 "<b>.</b>\n".format(
                                    first_name=html.escape(data.from_user.first_name),
                                    last_name=html.escape(data.from_user.last_name or ""),
                                    username=data.from_user.username,
                                    user_id=data.from_user.id,
                                    user_dc=data.from_user.dc_id,
                                    lang=data.from_user.language_code or "-",
                                    chat_title=message.chat.title,
                                    chat_username=message.chat.username,
                                    chat_id=message.chat.id,
                                    chat_type=message.chat.type
                                    ), parse_mode="HTML")
    except Exception as error:
        await send_error(client, error, "ids ids.py")
