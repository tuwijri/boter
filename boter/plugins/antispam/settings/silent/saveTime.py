from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, set_from_time_lock, set_to_time_lock
from boter.utils.scheduler import Permission
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("timeClose") | filters.regex("timeOpen"))
async def save_time(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("#")[2]
        status = button_data.split("#")[0]
        time = button_data.split("#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"EveryDay#{data}"
                        )
                    ],
                ]
            )
            await Permission(status, time.split(":")[0], time.split(":")[1], int(data), client)
            if status == "timeClose":
                status_group = f"**🔕 تم تعيين وقت القفل : {group_name}**"
                await set_from_time_lock(client, data, time)
            elif status == "timeOpen":
                status_group = f"**🔔 تم تعيين وقت الغاء القفل : {group_name}**"
                await set_to_time_lock(client, data, time)
            await callback_query.edit_message_text(f"{status_group}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "save_time saveTime.py")
