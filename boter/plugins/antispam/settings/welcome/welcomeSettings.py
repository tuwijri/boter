from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("welcome_settings_"))
async def welcome_group_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("welcome_settings_")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_data = await get_group_settings(client, data)
            group_name = group_data['group_name']
            welcome_status = group_data['welcome']['active']
            group_message = f"🎊--اعددات الترحيب :-- **{group_name}**\n-"
            welcome_active = "❌" if welcome_status == "NO" else "✅"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "📃 عرض الترحيب",
                            callback_data=f"showWelcome#{data}"
                        ),
                        InlineKeyboardButton(
                            "📝 تعديل الترحيب",
                            callback_data=f"editWelcome#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            f"♻️ حالة الترحيب : {welcome_active}",
                            callback_data=f"welcomeActive#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"groups_settings#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "welcome_group_settings welcomeSettings.py")
