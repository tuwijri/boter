from boter.boter import Boter
from pyrogram import filters
from boter.utils.database import check_group, get_media_state
from boter.utils.errors import send_error
from boter.utils.filters import GROUP_ADMINS, IS_ADMIN


@Boter.on_message(filters.left_chat_member & ~GROUP_ADMINS & ~IS_ADMIN & filters.group)
async def filter_left_service(client, message):
    try:
        chat_id = message.chat.id
        is_group = await check_group(client, chat_id)

        if is_group != "NO_GROUP":
            left_service = get_media_state(client, chat_id, "left_service")
            if left_service == "❌":
                try:
                    await message.delete()
                finally:
                    pass
    except Exception as error:
        await send_error(client, error, "filter_left_service left_service.py")
