import asyncio
import pyromod.listen
from pyrogram import Client
from pyrogram import __version__
from boter.config import API_ID, BOT_TOKEN, API_HASH, BOT_USERNAME
from boter.utils.check_timers import check_timer


class Boter(Client):

    def __init__(self):
        name = self.__class__.__name__.lower()

        super().__init__(
            name,
            api_id=API_ID,
            api_hash=API_HASH,
            bot_token=BOT_TOKEN,
            workers=16,
            plugins=dict(root=f"{name}/plugins"),
            workdir="."
        )

    async def start(self):
        await super().start()
        print(f"> Boter Started On @{BOT_USERNAME}\n> Pyrogram v{__version__}")
        # await check_timer(self)

    async def stop(self, *args):
        await super().stop()
