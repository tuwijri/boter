from pyrogram import filters
from boter.boter import Boter
from boter.config import CMD
from boter.utils.errors import send_error
from boter.utils.database import new_admin, remove_admin, bot_admins
from boter.utils.filters import IS_ADMIN


@Boter.on_message(filters.command(['اضافة_مشرف'], prefixes=CMD) & filters.reply & IS_ADMIN)
async def add_admin_reply(client, message):
    """
       اضافة مشرف للبوت
    """
    try:
        reply = message.reply_to_message  # معلومات الرسالة بالرد
        reply_user_id = reply.from_user.id  # ايدي العضو من الرسالة
        first_name = reply.from_user.first_name  # اسم العضو من الرسالة
        # اسم العضو الاخير من الرسالة
        last_name = reply.from_user.last_name if reply.from_user.last_name else "❌"
        # يوزر العضو من الرسالة
        user_name = reply.from_user.username if reply.from_user.username else "❌"
        forwards = reply.forward_from
        # اذا كانت الرسالة موجهة - اعادة توجيه
        if forwards:
            # اسم العضو
            forwards_name = reply.forward_from.first_name
            forwards_last = reply.forward_from.last_name if reply.forward_from.last_name else "❌"
            # يوزر العضو - معرف
            forwards_username = reply.forward_from.last_name if reply.forward_from.last_name else "❌"
            forwards_id = reply.forward_from.id  # ايدي العضو
            admin = await new_admin(client, forwards_id, forwards_username, forwards_name, forwards_last)
            await message.reply(admin)
        else:
            # اذا لم تكن الرسالة موجهة
            admin = await new_admin(client, reply_user_id, user_name, first_name, last_name)
            await message.reply(admin)
    except Exception as error:
        await send_error(client, error, "add_admin_reply admins.py")


@Boter.on_message(filters.command(['ازالة_مشرف'], prefixes=CMD) & filters.reply & IS_ADMIN)
async def remove_admin_reply(client, message):
    """
      ازالة مشرف من البوت
    """
    try:
        reply = message.reply_to_message  # معلومات الرسالة بالرد
        reply_user_id = reply.from_user.id  # ايدي العضو من الرسالة
        forwards = reply.forward_from
        # اذا كانت الرسالة موجهة - اعادة توجيه
        if forwards:
            forwards_id = reply.forward_from.id  # ايدي العضو
            admin = await remove_admin(client, forwards_id)
            await message.reply(admin)
        else:
            # اذا لم تكن الرسالة موجهة
            admin = await remove_admin(client, reply_user_id)
            await message.reply(admin)
    except Exception as error:
        await send_error(client, error, "remove_admin_reply admins.py")


@Boter.on_message(filters.command(['المشرفين'], prefixes=CMD) & IS_ADMIN)
async def list_bot_admins(client, message):
    try:
        admins = await bot_admins(client)
        await message.reply(admins)
    except Exception as error:
        await send_error(client, error, "list_bot_admins admins.py")
