from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("showRules"))
async def show_rules(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("showRules#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_data = await get_group_settings(client, data)
            group_name = group_data['group_name']
            rules_message = group_data['rules']['message']
            if rules_message != "NO_MESSAGE":
                group_message = f"📋--قوانين المجموعة :-- **{group_name}**\n\n{rules_message}\n-"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"rules_settings_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
            else:
                group_message = "**❌ لا يوجد قوانين**"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"rules_settings_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "show_rules showRules.py")
