from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, set_message_lock, get_message_lock, check_group_admins
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("messageLock"))
async def message_lock(client, callback_query):
    try:
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("messageLock#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"👥--اعدادات قفل المجموعة :-- **{group_name}**\n-"
            keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "📨 عرض جميع الرسائل",
                                callback_data=f"viwemessage#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🌔 رسالة فتح المجموعة اليومي 🔔",
                                callback_data=f"message#unlock_message#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🌒 رسالة قفل المجموعة اليومي 🔕",
                                callback_data=f"message#lock_message#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🕐 رسالة قفل المجموعة المؤقت 🔕",
                                callback_data=f"message#timer_lock_message#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "💰 الرسالة الإعلانية بعد القفل اليومي 🔕",
                                callback_data=f"message#ِad_lock_message#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"silent_settings#{data}"
                            )
                        ],
                    ]
                )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "message_lock messageLock.py")



@Boter.on_callback_query(filters.regex("viwemessage"))
async def viwe_message_lock(client, callback_query):
    try:
        
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("viwemessage#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        lock_message = await get_message_lock(client, data, "lock_message")
        unlock_message = await get_message_lock(client, data, "unlock_message")
        timer_lock_message = await get_message_lock(client, data, "timer_lock_message")
        ad_lock_message = await get_message_lock(client, data, "ad_lock_message")
        if is_group != "NO_GROUP":
            
            group_name = group_data['group_name']
            group_message = f"👥--رسائل قفل المجموعة :-- **{group_name}**-\n"
            message1 = await callback_query.message.reply(f"""
            **رسالة قفل اليومي للمجموعة:**
            {lock_message} 
            """)
            message2 = await callback_query.message.reply(f"""
            **رسالة فتح القفل اليومي للمجموعة:**
            {unlock_message}
            """)
            message3 = await client.send_message(check_group_admins(client),f"""
            **رسالة القفل المؤقت للمجموعة:**
            {timer_lock_message}
            """)
            message4 = await client.send_message(check_group_admins(client),f"""
            **الرسالة الإعلانية بعد القفل اليومي:**
            {ad_lock_message}
            """)     
            message1 = message1.id
            message2 = message2.id
            message3 = message3.id
            message4 = message4.id
            keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"messagesLockAndDel#{data}#{message1}#{message2}#{message3}#{message4}"
                            )
                        ],
                    ]
                )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "viwe_message_lock messageLock.py")


@Boter.on_callback_query(filters.regex("messagesLockAndDel"))
async def message_lock_and_del(client, callback_query):
    try:
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("#")[1]
        message1 = button_data.split("#")[2]
        message2 = button_data.split("#")[3]
        message3 = button_data.split("#")[4]
        message4 = button_data.split("#")[5]

        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            await client.delete_messages(
                chat_id=check_group_admins(client),
                message_ids=int(message1)
            )
            await client.delete_messages(
                chat_id=check_group_admins(client),
                message_ids=int(message2)
            )
            await client.delete_messages(
                chat_id=check_group_admins(client),
                message_ids=int(message3)
            )
            await client.delete_messages(
                chat_id=check_group_admins(client),
                message_ids=int(message4)
            )
            group_name = group_data['group_name']
            group_message = f"👥--اعدادات قفل المجموعة :-- **{group_name}**\n-"
            keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "📨 عرض جميع الرسائل",
                                callback_data=f"viwemessage#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🌔 رسالة فتح المجموعة اليومي 🔔",
                                callback_data=f"message#unlock_message#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🌒 رسالة قفل المجموعة اليومي 🔕",
                                callback_data=f"message#lock_message#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🕐 رسالة قفل المجموعة المؤقت 🔕",
                                callback_data=f"message#timer_lock_message#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "💰 الرسالة الإعلانية بعد القفل اليومي 🔕",
                                callback_data=f"message#ِad_lock_message#{data}"
                            )
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"silent_settings#{data}"
                            )
                        ],
                    ]
                )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "message_lock_and_del messageLock.py")



@Boter.on_callback_query(filters.regex("message#"))
async def text_message_lock(client, callback_query):
    try:
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("#")[2]
        tayp_message = button_data.split("#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_message = "⏳ ارسل نص الرسالة .."
            bot_reply = await callback_query.edit_message_text(f"{group_message}\n- أرسل الامر : /cancel للإلغاء\n-")
            reply = await message.chat.listen(filters.text & filters.user(user_id))
            text = reply.text
            bot_username = await client.get_me()
            cancel_word = [
                "/cancel", f"/cancel@{bot_username.username}",
                "#cancel", f"#cancel@{bot_username.username}",
                "#الغاء", f"#الغاء@{bot_username.username}",
                "/الغاء", f"/الغاء@{bot_username.username}",
                ""
            ]
            if text in cancel_word:
                message.chat.cancel_listener()
                await bot_reply.delete()
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"messageLock#{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply('**✅ تم الإلغاء**', reply_markup=keyboard)
            else:
                message = text
                group_id = data
                await bot_reply.delete()
                await set_message_lock(client, group_id, message, tayp_message)
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"messageLock#{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply('**تم ضبط الرسالة : **', reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "text_message_lock messageLock.py")
