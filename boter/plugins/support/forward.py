from pyrogram import Client, filters
import traceback

from boter.utils.errors import send_error
from boter.utils.filters import CHECK_COMMAND, CHECK_USER_BLOCKED
from boter.utils.database import check_group_admins, set_message_from_private


@Client.on_message(filters.private & ~CHECK_COMMAND & CHECK_USER_BLOCKED)
async def forward_messages_from_private(client, message):
    try:
        print(traceback.format_exc())
        id_group_admin = check_group_admins(client)
        if id_group_admin == "NO_GROUP":
            await message.reply("**🚫 التواصل متوقف حالياً**")
        else:
            message_forward = await client.forward_messages(id_group_admin, message.chat.id, message.id)
            await set_message_from_private(client, message, message_forward.id)
    except Exception as error:
        await send_error(client, error, "forward_messages_from_private forward.py")
