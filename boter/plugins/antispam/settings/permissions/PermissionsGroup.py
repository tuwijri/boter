# from boter.boter import Boter
from pyrogram import client, filters

from pyrogram.types import ChatPermissions
from boter.utils.errors import send_error
from boter.utils.database import set_permission_group, get_permission_group, get_message_lock, get_permission_group
from boter.config import CHANNEL_LOG
import json


async def time_permission(client, chat_id,
                          can_send_messages=None,
                          can_send_media_messages=None,
                          can_send_other_messages=None,
                          can_send_polls=None,
                          can_add_web_page_previews=None,
                          can_change_info=None,
                          can_invite_users=None,
                          can_pin_messages=None):
    await client.set_chat_permissions(
        chat_id,
        ChatPermissions(
            can_send_messages=can_send_messages,
            can_send_media_messages=can_send_media_messages,
            can_send_other_messages=can_send_other_messages,
            can_send_polls=can_send_polls,
            can_add_web_page_previews=can_add_web_page_previews,
            can_change_info=can_change_info,
            can_invite_users=can_invite_users,
            can_pin_messages=can_pin_messages
        )
    )


async def close_group(client, chat_id, message_type=None):
    try:
        ### تم التعديل ٢٠٢٢
        """
        لما اسحب البيانات من التيليقرام ما تجيني ديكشنري لذلك استدعيت مكتبة جيسون وحولتها دكشنري 
        بعدين حذفت اول كي 
        بعدها ارسلتها للداتا بيس علشان تسجلها
        """
        permission_group = await client.get_chat(chat_id)
        permissions_status = permission_group.permissions
        if permissions_status.can_send_messages == True:
            await client.set_chat_permissions(chat_id,ChatPermissions())
            if message_type == "lock_message":
                message = await get_message_lock(client, chat_id, "lock_message")
                ad_message = await get_message_lock(client, chat_id, "ad_lock_message")
                await client.send_message(chat_id, message)
                await client.send_message(chat_id, ad_message)
            elif message_type is None:
                message = await get_message_lock(client, chat_id, "timer_lock_message")
                await client.send_message(chat_id, message)
    except Exception as error:
        await send_error(client, error, "close_group PermissionsGroup.py")


async def open_group(client, chat_id, message_type=None):
    try:
        permission_group = await client.get_chat(chat_id)
        get_permissions = await get_permission_group(client, chat_id, 'can_send_messages')
        lock_status = await client.get_chat(chat_id)
        if get_permissions != "NO_GROUP" and lock_status.permissions.can_send_messages:
            await close_group(client, chat_id)

        # if get_permissions != "NO_GROUP" and not lock_status["permissions"]["can_send_messages"]:
        if get_permissions != "NO_GROUP" and not lock_status.permissions.can_send_messages:

            can_send_messages = await get_permission_group(client, chat_id, 'can_send_messages')
            can_send_media_messages = await get_permission_group(client, chat_id, 'can_send_media_messages')
            can_send_other_messages = await get_permission_group(client, chat_id, 'can_send_other_messages')
            can_send_polls = await get_permission_group(client, chat_id, 'can_send_polls')
            can_add_web_page_previews = await get_permission_group(client, chat_id, 'can_add_web_page_previews')
            can_change_info = await get_permission_group(client, chat_id, 'can_change_info')
            can_invite_users = await get_permission_group(client, chat_id, 'can_invite_users')
            can_pin_messages = await get_permission_group(client, chat_id, 'can_pin_messages')

            can_send_messages = True if can_send_messages == "✅" else False
            can_send_media_messages = True if can_send_media_messages == "✅" else False
            can_send_other_messages = True if can_send_other_messages == "✅" else False
            can_send_polls = True if can_send_polls == "✅" else False
            can_add_web_page_previews = True if can_add_web_page_previews == "✅" else False
            can_change_info = True if can_change_info == "✅" else False
            can_invite_users = True if can_invite_users == "✅" else False
            can_pin_messages = True if can_pin_messages == "✅" else False


            await time_permission(client, chat_id,
            can_send_messages=can_send_messages,
            can_send_media_messages=can_send_media_messages,
            can_send_other_messages=can_send_other_messages,
            can_send_polls=can_send_polls,
            can_add_web_page_previews=can_add_web_page_previews,
            can_change_info=can_change_info,
            can_invite_users=can_invite_users,
            can_pin_messages=can_pin_messages)


            if message_type == "unlock_message":
                message = await get_message_lock(client, chat_id, "unlock_message")
                await client.send_message(chat_id, message, parse_mode="Markdown")
        else:
            await client.send_message(CHANNEL_LOG, f"هناك خطاء في صلاحيات المجموعة او القروب مفتوح من قبل{chat_id}", parse_mode="Markdown")
    except Exception as error:
        await send_error(client, error, "open_group PermissionsGroup.py")