from pyrogram import Client, filters
from boter.config import CMD
from boter.plugins.antispam.settings.permissions.PermissionsGroup import close_group, open_group
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from pyrogram.types import ChatPermissions
from boter.utils.errors import send_error
from boter.utils.scheduler import Permission
from boter.utils.database import set_permission_group, get_permission_group
import json


@Client.on_message(filters.command(["test"], prefixes=CMD))
async def test(client, message):
    """
    لتجربة اي كود
    """
    try:
        group = "-1001185999760"
        await open_group(client, int(group))
        await message.reply("ok")
        # # await client.set_chat_permissions(-1001185999760, ChatPermissions())
        
        
        # group_message = f"⚙️-الاعدادات\n-"
        # keyboard = InlineKeyboardMarkup(
        #     [
        #         [
        #             InlineKeyboardButton(
        #                 text="اعدادات المجموعات",
        #                 callback_data=f"statusSilent#{group}"
        #             )
        #         ],
        #         [
        #             InlineKeyboardButton(
        #                 text="اعدادات البوت ",
        #                 callback_data=f"settings_bot"
        #             )
        #         ],
        #     ]
        # )
        # await message.reply(f"{group_message}", reply_markup=keyboard)



        
        # permission_group = await client.get_chat(message.chat.id)

        # can_send_messages = permission_group.permissions['can_send_messages']
        # can_send_media_messages = permission_group.permissions['can_send_media_messages']
        # can_send_stickers = permission_group.permissions['can_send_stickers']
        # can_send_animations = permission_group.permissions['can_send_animations']
        # can_send_games = permission_group.permissions['can_send_games']
        # can_use_inline_bots = permission_group.permissions['can_use_inline_bots']
        # can_add_web_page_previews = permission_group.permissions['can_add_web_page_previews']
        # can_send_polls = permission_group.permissions['can_send_polls']
        # can_change_info = permission_group.permissions['can_change_info']
        # can_invite_users = permission_group.permissions['can_invite_users']
        # can_pin_messages = permission_group.permissions['can_pin_messages']

        # if can_send_messages:
        #     print(can_send_messages)
        #     print(type(can_send_messages))
        #     await message.reply(can_send_messages)

    except Exception as error:
        print(f"test {error}")
        await send_error(client, error, "test test.py")
