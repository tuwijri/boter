from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.utils.errors import send_error
from boter.utils.filters import GROUP_ADMINS
from boter.utils.database import get_messages_database
from boter.boter import Boter
from boter.config import CMD

from boter.utils.util import build_menu


@Boter.on_callback_query(filters.regex("delete_admins_messages"))
async def delete_admins_messages(client, callback_query):
    try:
        messages = await get_messages_database(client)
        if messages != "NO_MESSAGES":
            button_list = []
            for row in messages:
                button_list.append(InlineKeyboardButton(row['message'], callback_data=f"to_delete_{row['message_id']}"))
            reply_markup = InlineKeyboardMarkup(
                await build_menu(button_list, n_cols=1))  # n_cols = 1 is for single column and multiple rows        
            await callback_query.edit_message_text("**📝 اختر الرسالة التي تريد حذفها : **\n**🔝 اعلى رسالة هي اخر رسالة مرسلة **\n.", reply_markup=reply_markup)
            
        else:
            await callback_query.edit_message_text("**⚠️ لا يوجد رسائل في البوت**")
    except Exception as error:
        await send_error(client, error, "delete_admins_messages delete.py")
