from pyrogram import filters
from boter.boter import Boter
from boter.config import CMD
from boter.utils.database import check_group_admins, set_group_admins, update_group_admins
from boter.utils.errors import send_error
from boter.utils.filters import IS_ADMIN


@Boter.on_message(filters.command(['مجموعة_المشرفين'], prefixes=CMD) & IS_ADMIN)
async def set_group(client, message):
    try:
        # التأكد اذا كانت مجموعة المشرفين موجودة او لا
        group = check_group_admins(client)
        if group == "NO_GROUP":
            new_group = await set_group_admins(client, message)
            await message.reply(new_group)
        else:
            new_group = await update_group_admins(client, message, group)
            await message.reply(new_group)
    except Exception as error:
        await send_error(client, error, "set_group setGroup.py")
