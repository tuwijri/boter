from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("EveryDay"))
async def silent_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("EveryDay#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_name = group_data['group_name']
            group_message = f"👥--اعدادات قفل المجموعة :-- **{group_name}**\n-"

            keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔕 تعيين وقت القفل",
                                callback_data=f"closeDay#{data}"
                            ),
                            InlineKeyboardButton(
                                "🔔 تعيين وقت الغاء القفل",
                                callback_data=f"openDay#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "❌ الغاء توقيت القفل",
                                callback_data=f"cancelDay#{data}"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"silent_settings#{data}"
                            )
                        ],
                    ]
                )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "silent_settings silentSettings.py")
