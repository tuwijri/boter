from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("allowed_words_"))
async def allowed_words_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("allowed_words_")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_data = await get_group_settings(client, data)
            group_name = group_data['group_name']
            allowed_status = group_data['white_list']['active']
            group_message = f"📜--اعدادات القائمة البيضاء :-- **{group_name}**\n-"
            allowed_active = "❌" if allowed_status == "NO" else "✅"

            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            f"♻️ حالة الكلمات :  {allowed_active}",
                            callback_data=f"allowedActive#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "🔖 الكلمات المسموح بها",
                            callback_data=f"listAllowedWords#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "➕",
                            callback_data=f"addAllowedWord#{data}"
                        ),
                        InlineKeyboardButton(
                            "➖",
                            callback_data=f"removeAllowedWord#{data}"
                        ),
                    ],
                    [
                        InlineKeyboardButton(
                            "⚠️ ازالة جميع الكلمات",
                            callback_data=f"removeAllAllowed#{data}"
                        )
                    ],
                    [
                        InlineKeyboardButton(
                            "🔙 رجوع",
                            callback_data=f"groups_settings#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "allowed_words_settings allowedWords.py")
