import re
from boter.boter import Boter
from pyrogram import filters
from boter.utils.database import check_group_admins, check_group, get_media_state
from boter.utils.errors import send_error
from boter.utils.filters import CHECK_LINK, GROUP_ADMINS, IS_ADMIN
from pyrogram.errors import UserNotParticipant



@Boter.on_message(CHECK_LINK & ~GROUP_ADMINS & ~IS_ADMIN & filters.group)
async def filter_link(client, message):
    try:
        user_id = message.from_user.id
        chat_id = message.chat.id
        is_group = await check_group(client, chat_id)
        link = get_media_state(client, chat_id, "link")
        telegram_link = get_media_state(client, chat_id, "telegram_link")
        if message.caption:
            is_url = re.findall(r"(?:t(?:elegram)?\.me|telegram\.org)\/(?P<username>[a-z0-9\_]{5,32})\/?", message.caption)
        else:
            is_url = re.findall(r"(?:t(?:elegram)?\.me|telegram\.org)\/(?P<username>[a-z0-9\_]{5,32})\/?", message.text)
        if is_group != "NO_GROUP":
            group_admins = check_group_admins(client)
            if group_admins != "NO_GROUP":
                try:
                    group_admins_users = await client.get_chat_member(group_admins, user_id)
                    user_group = group_admins_users.user.id
                    admins = await client.get_chat_member(chat_id, user_id)
                    if user_id != user_group or admins.status not in ['creator', 'administrator']:
                        if is_url:
                            if telegram_link == "❌":
                                try:
                                    await message.delete()
                                finally:
                                    pass
                        elif link == "❌":
                            try:
                                await message.delete()
                            finally:
                                pass
                except UserNotParticipant:
                    admins = await client.get_chat_member(chat_id, user_id)
                    if admins.status not in ['creator', 'administrator']:
                        if is_url:
                            if telegram_link == "❌":
                                try:
                                    await message.delete()
                                finally:
                                    pass
                        elif link == "❌":
                            try:
                                await message.delete()
                            finally:
                                pass
            else:
                admins = await client.get_chat_member(chat_id, user_id)
                if admins.status not in ['creator', 'administrator']:
                    if is_url:
                        if telegram_link == "❌":
                            try:
                                await message.delete()
                            finally:
                                pass
                    elif link == "❌":
                        try:
                            await message.delete()
                        finally:
                            pass
    except Exception as error:
        await send_error(client, error, "filter_link link.py")


