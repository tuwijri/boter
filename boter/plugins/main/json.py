import html
import os
from pyrogram import Client, filters
from boter.config import CMD
from boter.utils.errors import send_error
from ...utils.filters import IS_ADMIN


@Client.on_message(filters.command(["json"], prefixes=CMD) & IS_ADMIN)
async def json_dump(client, message):
    try:
        if len(str(message)) < 3000 and "-f" not in message.command:
            await message.reply_text(f"<code>{html.escape(str(message))}</code>", parse_mode="HTML")
        else:
            file_name = f"dump-{message.chat.id}.json"
            with open(file_name, "w") as f:
                f.write(str(message))
            await message.reply_document(file_name)
            os.remove(file_name)
    except Exception as error:
        await send_error(client, error, "json_dump json.py")
