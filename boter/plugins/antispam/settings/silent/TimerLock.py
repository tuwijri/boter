from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, get_message_lock
from boter.utils.errors import send_error
from boter.utils.scheduler import timer


@Boter.on_callback_query(filters.regex("TimerLock"))
async def Timer_Lock(client, callback_query):
    try:
        message = callback_query.message
        user_id = callback_query.from_user.id
        button_data = callback_query.data
        data = button_data.split("TimerLock#")[1]
        group_data = await get_group_settings(client, data)
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_message = "⏳ أرسل المدة الزمنية للاغلاق بالدقائق .."
            bot_reply = await callback_query.edit_message_text(f"{group_message}\n- أرسل الامر : /cancel للإلغاء\n-")
            reply = await message.chat.listen(filters.text & filters.user(user_id))
            text = reply.text
            bot_username = await client.get_me()
            cancel_word = [
                "/cancel", f"/cancel@{bot_username.username}",
                "#cancel", f"#cancel@{bot_username.username}",
                "#الغاء", f"#الغاء@{bot_username.username}",
                "/الغاء", f"/الغاء@{bot_username.username}",
                ""
            ]
            if text in cancel_word:
                message.chat.cancel_listener()
                await bot_reply.delete()
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"silent_settings#{data}"
                            )
                        ],
                    ]
                )
                await callback_query.message.reply('**✅ تم الإلغاء**', reply_markup=keyboard)
            else:
                Time = text
                if Time.isdigit():
                    await bot_reply.delete()
                    message_timer = await timer(int(Time), int(data), client)
                    keyboard = InlineKeyboardMarkup(
                        [
                            [
                                InlineKeyboardButton(
                                    "🔙 رجوع",
                                    callback_data=f"silent_settings#{data}"
                                )
                            ],
                        ]
                    )
                    await callback_query.message.reply(f'**{message_timer}**', reply_markup=keyboard)
                else:
                    message.chat.cancel_listener()
                    await bot_reply.delete()
                    keyboard = InlineKeyboardMarkup(
                        [
                            [
                                InlineKeyboardButton(
                                    "🔙 رجوع",
                                    callback_data=f"silent_settings#{data}"
                                )
                            ],
                        ]
                    )
                    await callback_query.message.reply('**⚠️ قم بإعادة القفل, أرسل رقم فقط**', reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "Timer_Lock TimerLock.py")
