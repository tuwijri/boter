from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("showWelcome"))
async def show_welcome(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("showWelcome#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_data = await get_group_settings(client, data)
            group_name = group_data['group_name']
            welcome_message = group_data['welcome']['message']
            if welcome_message != "NO_MESSAGE":
                group_message = f"📝--رسالة الترحيب :-- **{group_name}**\n\n{welcome_message}\n-"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"welcome_settings_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
            else:
                group_message = "**❌ لا يوجد رسالة ترحيب**"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"welcome_settings_{data}"
                            )
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "show_welcome showWelcome.py")
