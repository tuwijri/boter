from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, remove_all_allowed_words
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("removeAllAllowed"))
async def remove_all_allowed_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("removeAllAllowed#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            group_message = "**⚠️ حذف جميع الكلمات السموح بها :**"
            keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "❌ الغاء",
                            callback_data=f"allowed_words_{data}"
                        ),
                        InlineKeyboardButton(
                            "✅ تأكيد",
                            callback_data=f"deleteAllAllowed#{data}"
                        )
                    ],
                ]
            )
            await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "remove_all_allowed_settings removeAllAllowed.py")


@Boter.on_callback_query(filters.regex("deleteAllAllowed"))
async def delete_all_allowed(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("deleteAllAllowed#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            is_removed = await remove_all_allowed_words(client, data)
            if is_removed == "DONE":
                group_message = "**☑️ تم حذف جميع الكلمات المسموح بها**"
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"allowed_words_{data}"
                            ),
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
            else:
                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"allowed_words_{data}"
                            ),
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{is_removed}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "delete_all_allowed removeAllAllowed.py")