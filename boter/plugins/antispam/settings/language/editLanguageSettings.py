from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import get_language_settings, check_group, get_group_settings, change_language_setting
from boter.utils.errors import send_error


@Boter.on_callback_query(filters.regex("group_language#"))
async def edit_language_settings(client, callback_query):
    try:
        button_data = callback_query.data
        data = button_data.split("#")[2]
        media_type = button_data.split("#")[1]
        is_group = await check_group(client, data)
        if is_group != "NO_GROUP":
            change = await change_language_setting(client, data, media_type)
            if change == "DONE":
                ar = await get_language_settings(client, data, 'ar')
                en = await get_language_settings(client, data, 'en')
                fa = await get_language_settings(client, data, 'fa')
                it = await get_language_settings(client, data, 'it')
                br = await get_language_settings(client, data, 'br')
                zh = await get_language_settings(client, data, 'zh')
                fr = await get_language_settings(client, data, 'fr')
                tr = await get_language_settings(client, data, 'tr')
                de = await get_language_settings(client, data, 'de')
                hi = await get_language_settings(client, data, 'hi')
                he = await get_language_settings(client, data, 'he')

                group_data = await get_group_settings(client, data)
                group_name = group_data['group_name']
                group_message = f"💭--اعدادات اللغة :-- **{group_name}**\n\n✅ **: مسموح به**\n❌ **: ممنوع**\n-"

                keyboard = InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton(
                                ar,
                                callback_data=f"group_language#ar#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇸🇦 العربية",
                                callback_data=b"AR"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                en,
                                callback_data=f"group_language#en#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇬🇧 الانجليزية",
                                callback_data=b"EN"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                fa,
                                callback_data=f"group_language#fa#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇮🇷 الفارسية",
                                callback_data=b"FA"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                it,
                                callback_data=f"group_language#it#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇮🇹 ايطاليا",
                                callback_data=b"IT"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                br,
                                callback_data=f"group_language#br#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇧🇷 البرازيلية",
                                callback_data=b"BR"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                zh,
                                callback_data=f"group_language#zh#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇨🇳 الصينية",
                                callback_data=b"ZH"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                fr,
                                callback_data=f"group_language#fr#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇫🇷 الفرنسية",
                                callback_data=b"FR"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                tr,
                                callback_data=f"group_language#tr#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇹🇷 تركية",
                                callback_data=b"TR"
                            ),

                        ],
                        [
                            InlineKeyboardButton(
                                de,
                                callback_data=f"group_language#de#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇩🇪 الالمانية",
                                callback_data=b"DE"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                hi,
                                callback_data=f"group_language#hi#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇮🇳 الهندية",
                                callback_data=b"IN"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                he,
                                callback_data=f"group_language#he#{data}"
                            ),
                            InlineKeyboardButton(
                                "🇮🇱 العبرية",
                                callback_data=b"HE"
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "🔙 رجوع",
                                callback_data=f"groups_settings#{data}"
                            )
                        ],
                    ]
                )
                await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)
    except Exception as error:
        await send_error(client, error, "edit_language_settings editLanguage.py")