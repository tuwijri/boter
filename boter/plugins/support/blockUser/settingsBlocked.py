from pyrogram import filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from boter.boter import Boter
from boter.utils.database import check_group, get_group_settings, remove_all_block_user, user_block_lists
from boter.utils.errors import send_error
from boter.utils.filters import GROUP_ADMINS
from boter.config import CMD



@Boter.on_callback_query(filters.regex("blocked_settings"))
async def blocked_settings(client, callback_query):
    try:
        
        group_message = f"📜--اعدادات حظر المرسلين :-- \n\n\n\n لحظر عضو قم بالرد على رسالته بامر:-- \n #حظر او #block"
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        "🔖 قائمة المحظورين",
                        callback_data=f"blockUserLists"
                    ),
                ],
                [
                    InlineKeyboardButton(
                        "➖",
                        callback_data=f"removeAllowedWord"
                    ),
                ],
                [
                    InlineKeyboardButton(
                        "⚠️ ازالة جميع المحظورين",
                        callback_data=f"aaaaaaa"
                    )
                ],
                [
                    InlineKeyboardButton(
                        text=" ❌ الخروج من المحظورين",
                        callback_data="exit_Blocked"
                    )
                ],         
            ]
        )
        await callback_query.edit_message_text(f"{group_message}", reply_markup=keyboard)

    except Exception as error:
        await send_error(client, error, "blocked_settings settingsBlocked.py")

@Boter.on_callback_query(filters.regex("exit_Blocked"))
async def exit_Blocked(client, callback_query):
    """
       اغلاق  اعدادات المحظورين
    """
    try:
        group_message = "**☑️ تم اغلاق اعدادات المحظورين**"
        await callback_query.edit_message_text(f"{group_message}")
    except Exception as error:
        await send_error(client, error, "exit_Blocked settingsBlocked.py")



@Boter.on_callback_query(filters.regex("removeAllUserBlock"))
async def remove_All_user_Block(client, callback_query):
    """
       ازالة جميع المحظورين
    """
    try:
        group_message = await remove_all_block_user(client)
        await callback_query.edit_message_text(f"{group_message}")
    except Exception as error:
        await send_error(client, error, "remove_All_user_Block settingsBlocked.py")



@Boter.on_callback_query(filters.regex("blockUserLists"))
async def block_user_lists(client, callback_query):
    """
       قائمة جميع المحظورين
    """
    try:
        block_lists = await user_block_lists(client)
        if block_lists == "NO_USER_BLOCK":
            await callback_query.edit_message_text("**❌ لا يوجد محظورين**")
        else:
            await callback_query.edit_message_text("**👥 قائمة الأعضاء المحظورين : **", reply_markup=block_lists)
    except Exception as error:
        await send_error(client, error, "block_user_lists settingsBlocked.py")

