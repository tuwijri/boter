from pyrogram import filters
from boter.boter import Boter
from boter.config import CMD
from boter.utils.errors import send_error
from boter.utils.database import update_database
from boter.utils.filters import IS_ADMIN


@Boter.on_message(filters.command(['تحديث'], prefixes=CMD) & filters.group & IS_ADMIN)
async def update_groups(client, message):
    try:
        update = await update_database(client, message.chat.id)
        await message.reply(update)
    except Exception as error:
        await send_error(client, error, "update_groups update.py")
